package fr.umlv.andodab.contentProvider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;

public class AndodabContentProvider extends ContentProvider {

    // PROVIDER DATA
    static final String PROVIDER_NAME = "fr.umlv.andodab";
    static final String BASE_PATH = "andodabObject";
    static final String URL = "content://" + PROVIDER_NAME + "/";

    // TABLES
    static final String TABLE_OBJECT = "object";
    static final String TABLE_FLOAT_OBJECT = "float_obj";
    static final String TABLE_INTEGER_OBJECT = "integer_obj";
    static final String TABLE_STRING_OBJECT = "string_obj";
    static final String TABLE_COMMON_OBJECT = "common_obj";
    static final String TABLE_PARAMETER_OBJECT = "parameter_obj";

    // TABLES_COLUMN
    static final String TABLE_COLUMN_OBJECT_NAME = "name";
    static final String TABLE_COLUMN_OBJECT_TYPE = "type";
    static final String TABLE_COLUMN_OBJECT_SEAL = "seal";
    static final String TABLE_COLUMN_OBJECT_PARENTID = "parent_id";
    static final String TABLE_COLUMN_VAL = "val";
    static final String TABLE_COLUMN_ID = "id_obj";
    static final String TABLE_COLUMN_PARAMETER_KEY = "key";
    static final String TABLE_COLUMN_PARAMETER_TYPE = "typeOfSlaveParameter";
    static final String TABLE_COLUMN_PARAMETER_ID_MASTER = "id_master";
    static final String TABLE_COLUMN_PARAMETER_ID_SLAVE = "id_slave";
    // URI CODE
    static final int ANDODABOBJECTS = 0;
    static final int LAZYANDODABOBJECTS = 1;
    static final int PARAMETERS = 2;
    static final int ANDODABOBJECT_BY_ID = 3;
    static final int PARAMETER_BY_ID = 4;
    static final int PARAMETER_BY_ANDODABOBJECT_ID = 5;
    static final int LAZYANDODABOBJECT_BY_ID = 6;
    static final int FLOAT_OBJECT_BY_ID = 7;
    static final int INTEGER_OBJECT_BY_ID = 8;
    static final int STRING_OBJECT_BY_ID = 9;
    static final int LAZYANDODABOBJECT_CHILDREN_BY_PARENTID = 10;
    static final int PARAMETER_BY_SLAVE_ID = 11;
    static final int ANDODABOBJECT_ROOT = 12;


    // ID DU ROOT


    // URI MATCHER
    static final UriMatcher andodabURIMatcher;

    static {
        andodabURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH,
                ANDODABOBJECTS);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/lazy",
                LAZYANDODABOBJECTS);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/parameter",
                PARAMETERS);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/#",
                ANDODABOBJECT_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/parameter/#",
                PARAMETER_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/parameter/slave/#",
                PARAMETER_BY_SLAVE_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/parameter/object/#",
                PARAMETER_BY_ANDODABOBJECT_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/lazy/#",
                LAZYANDODABOBJECT_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/float/#",
                FLOAT_OBJECT_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/integer/#",
                INTEGER_OBJECT_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/string/#",
                STRING_OBJECT_BY_ID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/lazy/parent/#",
                LAZYANDODABOBJECT_CHILDREN_BY_PARENTID);
        andodabURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/root",
                ANDODABOBJECT_ROOT);

    }

    // DATABSE DATA
    private SQLiteDatabase database;

    static final String DATABASE_NAME = "andodabTest";

    static final int DATABASE_VERSION = 1;

    private static class AndodabDBSQLiteHelper extends SQLiteOpenHelper {

        public AndodabDBSQLiteHelper(Context context, String name,
                                     CursorFactory factory, int version,
                                     DatabaseErrorHandler errorHandler) {
            super(context, name, factory, version, errorHandler);

        }

        public AndodabDBSQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(SQLiteRequestFactory.createTableObject());
            db.execSQL(SQLiteRequestFactory.createTableStringObject());
            db.execSQL(SQLiteRequestFactory.createTableFloatObject());
            db.execSQL(SQLiteRequestFactory.createTableIntegerObject());
            db.execSQL(SQLiteRequestFactory.createTableCommonObject());
            db.execSQL(SQLiteRequestFactory.createTableParameterObject());

            String ID_DU_ROOT = new BigInteger(50, new Random()).toString();
            String ID_DU_FLOAT = new BigInteger(50, new Random()).toString();
            String ID_DU_STRING = new BigInteger(50, new Random()).toString();
            String ID_DU_INTEGER = new BigInteger(50, new Random()).toString();
            //CREATION DES OBJECTS PRIMITIFS DE BASE
            Logger.getAnonymousLogger().info(" === root created with " + ID_DU_ROOT);
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (" + ID_DU_ROOT + ",'root','R',1," + ID_DU_ROOT + ")");
            // PERE DES STRING
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES ("+ID_DU_STRING+",'string','S',1,+"+ID_DU_ROOT+")");
            db.execSQL("INSERT INTO string_obj (id_obj,name,val) VALUES ("+ID_DU_STRING+",'string','STRING')");
            // PERE DES FLOATS
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES ("+ID_DU_FLOAT+",'float','F',1,+"+ID_DU_ROOT+")");
            db.execSQL("INSERT INTO float_obj (id_obj,name,val) VALUES ("+ID_DU_FLOAT+",'float',-9999)");
            //PERE DES INTEGER
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES ("+ID_DU_INTEGER+",'integer','I',1,+"+ID_DU_ROOT+")");
            db.execSQL("INSERT INTO integer_obj (id_obj,name,val) VALUES ("+ID_DU_INTEGER+",'integer',-9999)");


            // OTHER ==============================
            //CREATION D'un FRUIT
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (1,'BANANE','C',0,+" + ID_DU_ROOT + ")");
            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (1,'BANANE')");
//            //CREATION DU FLOAT
            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (2,'energy','F',1,+"+ID_DU_FLOAT+")");
            db.execSQL("INSERT INTO float_obj (id_obj,name,val) VALUES (2,'energy',150.5)");
//            //CREATION Des parametres de fruits
            db.execSQL("INSERT INTO parameter_obj (id_obj,key,typeOfSlaveParameter,id_master,id_slave) VALUES (0,'Valeur_Calorifique','DEFINE_OBJECT',1,2)");
//
//
            //CREATION De banane _verte
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (3,'BANANE_VERTE','C',0,1)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (3,'BANANE_VERTE')");
//
//            //CREATION DE BANANE VERTE FONCE
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (4,'BANANE_VERTE_FONCE','C',0,3)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (4,'BANANE_VERTE_FONCE')");
//
//            //CREATION DE ANIMAL
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (5,'ANIMAL','C',0,"+ID_DU_ROOT+")");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (5,'ANIMAL')");
//            //CREATION Des parametres de animal
//            db.execSQL("INSERT INTO parameter_obj (id_obj,key,typeOfSlaveParameter,id_master,id_slave) VALUES (3,'fruit','DEFINE_OBJECT',5,3)");
//
//            //CREATION DE KOALA
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (6,'KOALA','C',0,5)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (6,'KOALA')");
//            //CREATION Des parametres de animal
//            db.execSQL("INSERT INTO parameter_obj (id_obj,key,typeOfSlaveParameter,id_master,id_slave) VALUES (2,'fruit','DEFINE_OBJECT',6,4)");
//
//            //PARAMETRE POUR TESTE
//            // CREATION DE BANANE VERTE FONCE
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (7,'BANANE_VERTE_FONCE_MARRON','C',0,1)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (7,'BANANE_VERTE_FONCE_MARRON')");
//
//            //PARAMETRE POUR TESTE
//            // CREATION DE BANANE VERTE FONCE
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (8,'BANANE_VERTE_FONCE_JAUNE','C',0,7)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (8,'BANANE_VERTE_FONCE_JAUNE')");
//
//            //PARAMETRE POUR TESTE
//            // CREATION DE BANANE VERTE FONCE
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (9,'BANANE_VERTE_FONCE_BLUE','C',0,8)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (9,'BANANE_VERTE_FONCE_BLUE')");
//
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (10,'BANANE_VERTE_FONCE_BLUE_MARINE','C',0,8)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (10,'BANANE_VERTE_FONCE_BLUE_MARINE')");
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (11,'BANANE_VERTE_FONCE_BLUE_MARINE_FILS','C',0,10)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (11,'BANANE_VERTE_FONCE_BLUE_MARINE_FILS')");
//
//            db.execSQL("INSERT INTO object (id_obj,name,type,seal,parent_id) VALUES (12,'BANANE_VERTE_FONCE_BLUE_MARINE_FILS_2','C',0,11)");
//            db.execSQL("INSERT INTO common_obj (id_obj,name) VALUES (12,'BANANE_VERTE_FONCE_BLUE_MARINE_FILS_2')");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARAMETER_OBJECT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FLOAT_OBJECT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTEGER_OBJECT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STRING_OBJECT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMON_OBJECT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMON_OBJECT);
            onCreate(db);
        }


        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            super.onOpen(db);
            if (!db.isReadOnly()) {
                // Enable foreign key constraints
                db.execSQL("PRAGMA foreign_keys=ON;");
                Logger.getAnonymousLogger().info("PRAGMA ON DONE");
            }
        }
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        AndodabDBSQLiteHelper dbHelper = new AndodabDBSQLiteHelper(context);
        /**
         * Create a write able database which will trigger its creation if it
         * doesn't already exist.
         */
        database = dbHelper.getWritableDatabase();

        return (database == null) ? false : true;

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (andodabURIMatcher.match(uri)) {

            case ANDODABOBJECTS:
                queryBuilder.setTables("object");
                break;
            case LAZYANDODABOBJECTS:
                queryBuilder.setTables("object");
                break;
            case PARAMETERS:
                queryBuilder.setTables("parameter_obj");
                break;

            case ANDODABOBJECT_BY_ID:
                queryBuilder.setTables("object");
                queryBuilder.appendWhere("id_obj=" + uri.getLastPathSegment());
                break;
            case PARAMETER_BY_ANDODABOBJECT_ID:
                queryBuilder.setTables("parameter_obj");
                queryBuilder.appendWhere("id_master=" + uri.getLastPathSegment());
                break;
            case PARAMETER_BY_ID:
                queryBuilder.setTables("parameter_obj");
                queryBuilder.appendWhere("id_obj=" + uri.getLastPathSegment());
                break;
            case PARAMETER_BY_SLAVE_ID:
                queryBuilder.setTables("parameter_obj");
                queryBuilder.appendWhere("id_slave=" + uri.getLastPathSegment());
                break;
            case LAZYANDODABOBJECT_BY_ID:
                queryBuilder.setTables("object");
                queryBuilder.appendWhere("id_obj=" + uri.getLastPathSegment());
                break;
            case FLOAT_OBJECT_BY_ID:
                queryBuilder.setTables("float_obj");
                queryBuilder.appendWhere("name='float'");
                break;
            case INTEGER_OBJECT_BY_ID:
                queryBuilder.setTables("integer_obj");
                queryBuilder.appendWhere("name='integer'");
                break;
            case STRING_OBJECT_BY_ID:
                queryBuilder.setTables("string_obj");
                queryBuilder.appendWhere("name='string'" );
                break;
            case LAZYANDODABOBJECT_CHILDREN_BY_PARENTID:
                queryBuilder.setTables("object");
                queryBuilder.appendWhere("parent_id=" + uri.getLastPathSegment());
                break;
            case ANDODABOBJECT_ROOT:
                queryBuilder.setTables("object");
                queryBuilder.appendWhere("parent_id=" + "id_obj");
                break;
            default:

                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }

        return queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, sortOrder);
    }


    @Override
    public String getType(Uri uri) {
        switch (andodabURIMatcher.match(uri)) {

            case ANDODABOBJECTS:
                return "vnd.android.cursor.dir/vnd.andodab.object.all";
            case LAZYANDODABOBJECTS:
                return "vnd.android.cursor.dir/vnd.andodab.object.lazy.all";
            case PARAMETERS:
                return "vnd.android.cursor.dir/vnd.andodab.parameter.all";
            case ANDODABOBJECT_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.object.single";
            case PARAMETER_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.parameter.single";
            case PARAMETER_BY_ANDODABOBJECT_ID:
                return "vnd.android.cursor.item/vnd.andodab.parameter.object.single";
            case LAZYANDODABOBJECT_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.object.lazy.single";
            case FLOAT_OBJECT_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.object.flaot.value.single";
            case INTEGER_OBJECT_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.object.integer.value.single";
            case STRING_OBJECT_BY_ID:
                return "vnd.android.cursor.item/vnd.andodab.object.string.value.single";
            case LAZYANDODABOBJECT_CHILDREN_BY_PARENTID:
                return "vnd.android.cursor.dir/vnd.andodab.lazy.parent.all";
            case ANDODABOBJECT_ROOT:
                return "vnd.android.cursor.item/vnd.andodab.object.root.value.single";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (andodabURIMatcher.match(uri)) {
//            case LAZYANDODABOBJECTS:
//                return insertLazyAndodabObject(uri, values);
            case ANDODABOBJECTS:
                String val = (String) values.get(TABLE_COLUMN_VAL);
                Logger.getAnonymousLogger().info("Value = " + values.get(TABLE_COLUMN_ID));
                values.remove(TABLE_COLUMN_VAL);
                Uri uriResult = insertLazyAndodabObject(uri, values);

                //   values.put(TABLE_COLUMN_ID, uriResult.getLastPathSegment());
                values.put(TABLE_COLUMN_VAL, val);
                return insertAndodabObject(uri, values);

            case PARAMETERS:
                return insertParameterObject(uri, values);

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }


    }

    /**
     * insert in object table
     *
     * @param uri
     * @param values
     * @return
     */
    private Uri insertLazyAndodabObject(Uri uri, ContentValues values) {


        long rowID = database.insert(TABLE_OBJECT, "", values);

        if (rowID > 0) {
            String urlToReturn = URL + "/INSERT_LAZY/";
            Uri uriToReturn = Uri.parse(urlToReturn);
            Uri _uri = ContentUris.withAppendedId(uriToReturn, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        //replace it by your own exception maybe
        throw new SQLException("Failed to add a record into " + uri);
    }

    private Uri insertParameterObject(Uri uri, ContentValues values) {
        Logger.getAnonymousLogger().info("INSERT PARAMETER OBJECT");
//CHECK INTEGRITY BEFORE INSERTING

        try {
            AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());
            String key = (String) values.get(TABLE_COLUMN_PARAMETER_KEY);
            String id_master = (String) values.get(TABLE_COLUMN_PARAMETER_ID_MASTER);
            String id_slave = (String) values.get(TABLE_COLUMN_PARAMETER_ID_SLAVE);
            AndodabObject andoMaster = andodabDAO.getAndodabObectById(id_master);
            AndodabObject andoSlave = andodabDAO.getAndodabObectById(id_slave);

            //AndodabParameter andoParameterChild = andoSlave.getAndoParameterByKey(key);
            if (andoMaster.isSealed()) {
                throw new SQLException("L'object est scellé: impossible d'inserer un élement");
            }
            // 1. JE CHERHCE L'ancetre ayant la meme clé
            String id = andoMaster.getParentId();
            Logger.getAnonymousLogger().info(" ID INSERT = " + id);
            while (!id.equals(andodabDAO.getRootID())) {

                AndodabParameter andoParameter = andodabDAO.getAndodabParameterByKeyAndIdMaster(id, key);
                // je viens de trouver un ancetre ayant la clé
                Logger.getAnonymousLogger().info("ANDOPARAMETER == " + (andoParameter == null));
                if (andoParameter != null) {
                    // je teste si le parametre de l'ancetre est plus générale ou égale au parametre que je veux insérer
                    boolean result = andodabDAO.isAndoChildHeritedFromAndoParent(andoParameter.getSlave(), andoSlave.toLazyAndodabObject());
                    Logger.getAnonymousLogger().info("Result == " + result);
                    if (!result) {
                        throw new SQLException("Le parametre a inserer  viole la contrainte de specialisation : il doit etre " +
                                "= ou plus specialisé que celui du père");
                    }
                    id = andodabDAO.getRootID();
                    break;
                } else {
                    // je recupère l'ancetre de l'ancetre ...
                    AndodabObject andodabObjectParent = andodabDAO.getAndodabObectById(id);
                    id = andodabObjectParent.getParentId();

                }

            }

        } catch (NotFoundElementException e) {
            throw new SQLException("Integrity error ");
        }

        long rowID = database.insert(TABLE_PARAMETER_OBJECT, "", values);
        if (rowID > 0) {
            String urlToReturn = URL + "/INSERT_PARAMETER/";
            Uri uriToReturn = Uri.parse(urlToReturn);
            Uri _uri = ContentUris.withAppendedId(uriToReturn, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        //replace it by your own exception maybe
        throw new SQLException("Failed to add a record into " + uri);
    }

    /**
     * insert in andodab tables
     *
     * @param uri
     * @param values
     * @return
     */
    private Uri insertAndodabObject(Uri uri, ContentValues values) {

        String type = (String) values.get(TABLE_COLUMN_OBJECT_TYPE);
        long rowID = -1;
        values.remove(TABLE_COLUMN_OBJECT_SEAL);
        values.remove(TABLE_COLUMN_OBJECT_PARENTID);
        Logger.getAnonymousLogger().info("type =" + type);
        if (type.equals("C")) {
            values.remove(TABLE_COLUMN_VAL);
            rowID = database.insert(TABLE_COMMON_OBJECT, "", values);
        } else if (type.equals("F")) {
            String floatVal = (String) values.get(TABLE_COLUMN_VAL);
            values.remove(TABLE_COLUMN_VAL);
            values.put(TABLE_COLUMN_VAL, new Float(floatVal));
            rowID = database.insert(TABLE_FLOAT_OBJECT, "", values);
        } else if (type.equals("I")) {
            String integerVal = (String) values.get(TABLE_COLUMN_VAL);
            values.remove(TABLE_COLUMN_VAL);
            values.put(TABLE_COLUMN_VAL, new Integer(integerVal));
            rowID = database.insert(TABLE_INTEGER_OBJECT, "", values);
        } else if (type.equals("S")) {
            rowID = database.insert(TABLE_STRING_OBJECT, "", values);
        } else {
            throw new SQLException("Bad Request : this type can not be insert with this methode" + uri);
        }
        if (rowID > 0) {
            String urlToReturn = URL + "/INSERT_ANDODAB/";
            Uri uriToReturn = Uri.parse(urlToReturn);
            Uri _uri = ContentUris.withAppendedId(uriToReturn, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        //replace it by your own exception maybe
        throw new SQLException("Failed to add a record into " + uri);

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());
        switch (andodabURIMatcher.match(uri)) {
            case ANDODABOBJECT_BY_ID:
                String idOfAndo = uri.getLastPathSegment();
                if (idOfAndo.equals(andodabDAO.getRootID())) {
                    throw new IllegalArgumentException("you can not delete the root object");
                }
                return database.delete(TABLE_OBJECT, TABLE_COLUMN_ID + "=" + idOfAndo, selectionArgs);
            case PARAMETER_BY_ID:
                String idOfAndoParam = uri.getLastPathSegment();

                return database.delete(TABLE_PARAMETER_OBJECT, TABLE_COLUMN_ID + "=" + idOfAndoParam, selectionArgs);
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());
//        String mSelectionClause = AndodabContentProvider.TABLE_COLUMN_ID+"=?";
//        String[] mSelectionArgs = {uri.getLastPathSegment()};
        switch (andodabURIMatcher.match(uri)) {
            case ANDODABOBJECT_BY_ID:
                String idOfAndo = uri.getLastPathSegment();
                if (idOfAndo.equals(andodabDAO.getRootID())) {
                    throw new IllegalArgumentException("you can not update the root object");
                }
                return updateAndodabObject(idOfAndo, uri, values, selection, selectionArgs);
            case PARAMETER_BY_ID:
                String idOfAndoParam = uri.getLastPathSegment();
                if (idOfAndoParam.equals(andodabDAO.getRootID())) {
                    throw new IllegalArgumentException("you can not update the root object");
                }
                return updateAndodabParameter((idOfAndoParam), uri, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private int updateAndodabParameter(String id, Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String key = (String) values.get(TABLE_COLUMN_PARAMETER_KEY);
        String masterId = (String) values.get(TABLE_COLUMN_PARAMETER_ID_MASTER);
        Logger.getAnonymousLogger().info("master =" + masterId);
        String new_slave_id = (String) values.get(TABLE_COLUMN_PARAMETER_ID_SLAVE);
        AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());

        //Regarder si pour les parents possedant la clé
        //la nouvelle valeur du parametre
        //ne viole pas l'intégrité
        boolean result = (isNewParamRespectAndodabIntegrityWithHisParents(id, new_slave_id, key, masterId) && isNewParamRespectAndodabIntegrityWithHisChildren(id, new_slave_id, key, masterId));
        if (!result) {
            throw new SQLException("Le modification ne respecte pas la contrainte d'integrite");
        }

        return database.update(TABLE_PARAMETER_OBJECT, values, selection, selectionArgs);
    }

    private boolean isNewParamRespectAndodabIntegrityWithHisParents(String id, String new_slave_id, String key, String masterId) {
        try {
            AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());
            LazyAndodabObject master = andodabDAO.getLazyAndodabObjectById(masterId);
            String id_master = master.getParentId();
            Logger.getAnonymousLogger().info("param Master Id = " + master.getId() + " parent id =" + id_master);
            while (!id_master.equals(andodabDAO.getRootID())) {
                // recherche ancetre ayant la cle
                AndodabParameter andoParameter = andodabDAO.getAndodabParameterByKeyAndIdMaster(id_master, key);
                // je viens de trouver un ancetre ayant la clé
                Logger.getAnonymousLogger().info("ANDOPARAMETER UPDATE == " + (andoParameter == null));
                if (andoParameter != null) {
                    // je teste si le parametre de l'ancetre est plus générale ou égale au parametre que je veux insérer
                    boolean result = andodabDAO.isAndoChildHeritedFromAndoParent(andoParameter.getSlave().getId(), id, new_slave_id);
                    Logger.getAnonymousLogger().info("Result == " + result);
                    if (!result) {
                        throw new SQLException("L'id parent viole la contrainte");
                    }
                    id_master = andodabDAO.getRootID();
                    return true;
                } else {
                    // je recupère l'ancetre de l'ancetre ...
                    AndodabObject andodabObjectParent = andodabDAO.getAndodabObectById(id_master);
                    id_master = andodabObjectParent.getParentId();

                }
            }
            return true;
        } catch (NotFoundElementException e) {
            return false;
        }
    }

    private boolean isNewParamRespectAndodabIntegrityWithHisChildren(String id, String new_slave_id, String key, String masterId) {
        try {
            AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());
            List<LazyAndodabObject> lazyandoList = andodabDAO.getLazyAndodabObjectChildrenByParentId(masterId);
            for (LazyAndodabObject lazyando : lazyandoList) {
                AndodabObject ando = andodabDAO.getAndodabObectById(lazyando.getId());
                AndodabParameter andoParameter = ando.getAndoParameterByKey(key);
                if (andoParameter == null) {
                    return isNewParamRespectAndodabIntegrityWithHisParents(id, new_slave_id, key, ando.getId());
                } else {
                    LazyAndodabObject slave = andoParameter.getSlave();
                    boolean result = andodabDAO.isAndoChildHeritedFromAndoParent(andodabDAO.getLazyAndodabObjectById(new_slave_id), slave);
                    if (!result) {
                        return false;
                    }
                }
            }
        } catch (NotFoundElementException e) {
            return false;
        }
        return true;
    }

    private int updateAndodabObject(String id, Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        AndodabDAO andodabDAO = new AndodabDAO(getContext().getContentResolver(),getContext());

        try {
            String new_parent_id = (String) values.get("parent_id");
            //Si j'ai change le parent id
            // je verifie que l'integrité n'est pas mis en danger
            if (new_parent_id.equals(id)) {
                throw new SQLException("L'id de l'objet et de celui du parent ne peuvent pas etre identique");
            }
            if (new_parent_id != null) {
                LazyAndodabObject lazySlave = andodabDAO.getLazyAndodabObjectById(id);
                List<AndodabParameter> parameterList = andodabDAO.getAndodabParameterBySlaveId(id);
                Logger.getAnonymousLogger().info("paramList = " + parameterList.size());
                for (AndodabParameter parameter : parameterList) {
                    LazyAndodabObject master = parameter.getMaster();
                    String id_master = master.getParentId();
                    Logger.getAnonymousLogger().info("param Master Id = " + master.getId() + " parent id =" + id_master);
                    while (id_master.equals(andodabDAO.getRootID())) {
                        // recherche ancetre ayant la cle
                        AndodabParameter andoParameter = andodabDAO.getAndodabParameterByKeyAndIdMaster(id_master, parameter.getKey());
                        // je viens de trouver un ancetre ayant la clé
                        Logger.getAnonymousLogger().info("ANDOPARAMETER UPDATE == " + (andoParameter == null));
                        if (andoParameter != null) {
                            // je teste si le parametre de l'ancetre est plus générale ou égale au parametre que je veux insérer
                            boolean result = andodabDAO.isAndoChildHeritedFromAndoParent(andoParameter.getSlave().getId(), id, new_parent_id);
                            Logger.getAnonymousLogger().info("Result == " + result);
                            if (!result) {
                                throw new SQLException("L'id parent viole la contrainte");
                            }
                            id_master = andodabDAO.getRootID();
                            break;
                        } else {
                            // je recupère l'ancetre de l'ancetre ...
                            AndodabObject andodabObjectParent = andodabDAO.getAndodabObectById(id_master);
                            id_master = andodabObjectParent.getParentId();

                        }
                    }
                }
            }

            //TODO REMOVE SOME VALUES
            // removes val because common hasn't value
            String updatedVal = (String) values.get(TABLE_COLUMN_VAL);
            values.remove(TABLE_COLUMN_VAL);
            Logger.getAnonymousLogger().info(" UPDATE VALUES HAS " + values.size());
            //MIS A JOUR DE LA PARTIE TABLE OBJET
            int result = database.update(TABLE_OBJECT, values, selection, selectionArgs);
            Logger.getAnonymousLogger().info(" result =  " + result + " selection = " + selection + " selectionArgs =" + selectionArgs[0]);
            // removes parent id
            values.remove(TABLE_COLUMN_OBJECT_PARENTID);


            //MIS A JOUR DE LA BONNE TABLE OBJET CORRESPONDANTE
            if (result != 0) {
                String type = (String) values.get(TABLE_COLUMN_OBJECT_TYPE);
                if (type.equals("C")) {

                    result = database.update(TABLE_COMMON_OBJECT, values, selection, selectionArgs);
                } else if (type.equals("F")) {

                    values.put(TABLE_COLUMN_VAL, new Float(updatedVal));
                    result = database.update(TABLE_FLOAT_OBJECT, values, selection, selectionArgs);
                } else if (type.equals("I")) {

                    values.put(TABLE_COLUMN_VAL, new Integer(updatedVal));
                    result = database.update(TABLE_INTEGER_OBJECT, values, selection, selectionArgs);
                } else if (type.equals("S")) {
                    values.put(TABLE_COLUMN_VAL, updatedVal);
                    result = database.update(TABLE_STRING_OBJECT, values, selection, selectionArgs);
                } else {
                    throw new SQLException("Bad Request : this type can not be insert with this methode" + uri);
                }
                return result;
            } else {
                return result;
            }
        } catch (NotFoundElementException e) {
            throw new SQLException("object introuvable");
        }

    }

}
