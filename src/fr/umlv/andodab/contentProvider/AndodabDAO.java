package fr.umlv.andodab.contentProvider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import fr.umlv.andodab.contentProvider.AndodabObject.ObjectType;
import fr.umlv.andodab.contentProvider.exception.DataBaseIntegrityException;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;
import fr.umlv.andodab.synchronization.EventRecorder;
import fr.umlv.andodab.synchronization.EventUtilsCreator;
import fr.umlv.andodab.synchronization.ObjectCreationEvent;
import fr.umlv.andodab.synchronization.ObjectDeletionEvent;
import fr.umlv.andodab.synchronization.ObjectUpdateEvent;
import fr.umlv.andodab.synchronization.ParameterCreationEvent;
import fr.umlv.andodab.synchronization.ParameterDeletionEvent;
import fr.umlv.andodab.synchronization.ParameterUpdateEvent;

/**
 * This object is use to manage access to the database
 *
 * @author emmanuel
 */
public class AndodabDAO {
  private ContentResolver contentResolver;
  private Context context;
  private EventRecorder eventRecorder;


  public AndodabDAO(ContentResolver contentResolver, Context context) {
    this.contentResolver = contentResolver;
    this.context = context;
    try {
      this.eventRecorder = EventUtilsCreator.getEventRecorder(context);
    } catch (IOException e) {
      this.eventRecorder = null;
    }
  }

  public Context getContext() {
    return context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  /**
   * retreive the minimal info of all object in the database this can help to
   * show a global and quick view of the database
   *
   * @return
   */
  public List<LazyAndodabObject> getAllLazyAndodabObject() {
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH;

    Uri uriToParse = Uri.parse(url);
    Cursor c = this.contentResolver.query(uriToParse, null, null, null,
      "id_obj");

    List<LazyAndodabObject> lazyAndoList = new ArrayList<LazyAndodabObject>();

    if (!c.moveToFirst()) {
      c.close();
      return lazyAndoList;
    } else {
      do {
        String id_obj = c.getString(c.getColumnIndex("id_obj"));

        if (!id_obj.equals(getRootID()) && !id_obj.equals(getINTEGERID()) && !id_obj.equals(getFLOATID()) && !id_obj.equals(getSTRINGID())) {
          String name = c.getString(c.getColumnIndex("name"));
          String strType = c.getString(c.getColumnIndex("type"));
          int intSeal = c.getInt(c.getColumnIndex("seal"));
          String parent_Id = c.getString(c.getColumnIndex("parent_id"));
          boolean isSeal = intSeal == 1;
          LazyAndodabObject lazyAndo = new LazyAndodabObject(id_obj, name, ObjectType.valueOf(strType), parent_Id, isSeal);
          lazyAndoList.add(lazyAndo);
        }

      } while (c.moveToNext());

    }
    c.close();
    return lazyAndoList;
  }

    /**
     * this will get all the info of the element from the database by his id
     *
     * @param id
     * @return
     * @throws NotFoundElementException
     */
    public AndodabObject getAndodabObectById(String id)
            throws NotFoundElementException {
        //I GET ANDODAB BY ID FIRST
        //AND THEN I WILL LOOK FOR PARAMETER

        String andodabURL = AndodabContentProvider.URL
                + AndodabContentProvider.BASE_PATH + "/" + id;

        Uri uriToParse = Uri.parse(andodabURL);
        Cursor c = this.contentResolver.query(uriToParse, null, null, null,
                "name");
        // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
        if (!c.moveToFirst()) {
            throw new NotFoundElementException();
        } else {
            String id_obj = c.getString(c.getColumnIndex("id_obj"));
            String name = c.getString(c.getColumnIndex("name"));
            String strType = c.getString(c.getColumnIndex("type"));
            int intSeal = c.getInt(c.getColumnIndex("seal"));
            String parent_Id = c.getString(c.getColumnIndex("parent_id"));
            boolean isSeal = intSeal == 1;
            AndodabObject ando = new AndodabObject(id_obj, name,
                    ObjectType.valueOf(strType), parent_Id, isSeal);

            //set value of andodab object
            if (!(ando.getType().toString().equals("R") || ando.getType().toString().equals("C"))) {
                String valueURL = "";
                valueURL = AndodabContentProvider.URL
                        + AndodabContentProvider.BASE_PATH;


                String[] projection = {"val"};
                // get float value
                if (ando.getType().toString().equals("F")) {
                    valueURL += "/float/";
                }
                // get integer value
                else if (ando.getType().toString().equals("I")) {
                    valueURL += "/integer/";
                }
                // get string value
                else if (ando.getType().toString().equals("S")) {
                    valueURL += "/string/";
                }
                valueURL += id;
                Uri valueUriToParse = Uri.parse(valueURL);

                Cursor valueCursor = this.contentResolver.query(valueUriToParse, projection, null, null, null);

                if (valueCursor.moveToFirst()) {
                    String val = valueCursor.getString(valueCursor.getColumnIndex("val"));
                    ando.setValue(val);

                }

            }

            List<AndodabParameter> parameterList = constructParameterOfAndodabObject(id, ando.getParentId());

            ando.getParams().addAll(parameterList);
            c.close();
            return ando;
        }


    }

    private List<AndodabParameter> constructParameterOfAndodabObject(String id, String parent_id) throws NotFoundElementException {
        //NOW I RETREIVE NOT HERITED PARAMETER
        List<AndodabParameter> notHeritedParams = getParametersByAndodabObjectId(id);

        // NOW I GET A LIST OF ALL PARAMETER
        List<AndodabParameter> heritedParams = getAllParameter(parent_id, notHeritedParams);
        //   ando.getParams().addAll(notHeritedParams);
        return heritedParams;
    }

    /**
     * this method is a little bit stricky
     * it will return all the parameter of an object
     * herited and not
     * it use the notHeritedParams to know if the child have redifned object
     * an object is consider to be redifined if it is in the notHeritedParams and in one of the parent of that object
     * <p/>
     * so all param = notHerited + param of andodab parent : not redifened in notHeritedParams
     *
     * @param id
     * @param notHeritedParams
     * @return
     */
    public List<AndodabParameter> getAllParameter(String id, List<AndodabParameter> notHeritedParams) throws NotFoundElementException {

//        Map<String, AndodabParameter> parameterMap = new HashMap<>();
//        for(AndodabParameter parameter : notHeritedParams){
//            parameterMap.put(parameter.getKey(),parameter);
//        }
//
//        long id_parent = id;
//        do {
//            LazyAndodabObject lazyAndodabObject = getLazyAndodabObjectById(id_parent);
//            List<AndodabParameter> parameterList = getParametersByAndodabObjectId(lazyAndodabObject.getId());
//            //ADD THE NEARLEST HERITED
//            for(AndodabParameter andodabParameter : parameterList){
//                String key = andodabParameter.getKey();
//                boolean isAlreadyIn = parameterMap.containsKey(key);
//                if(!isAlreadyIn){
//                    parameterMap.put(key, andodabParameter);
//                }
//            }
//            id_parent=lazyAndodabObject.getParentId();
//    Logger.getAnonymousLogger().info("id parent = "+id_parent);
//        }while(id_parent !=0);


    return new ArrayList<>(getAllParameterMap(id, notHeritedParams).values());
  }

  private Map<String, AndodabParameter> getAllParameterMap(String id, List<AndodabParameter> notHeritedParams) throws NotFoundElementException {

    Map<String, AndodabParameter> parameterMap = new HashMap<>();
    for (AndodabParameter parameter : notHeritedParams) {
      parameterMap.put(parameter.getKey(), parameter);
    }

    String id_parent = id;
    do {
      LazyAndodabObject lazyAndodabObject = getLazyAndodabObjectById(id_parent);
      List<AndodabParameter> parameterList = getParametersByAndodabObjectId(lazyAndodabObject.getId());
      //ADD THE NEARLEST HERITED
      for (AndodabParameter andodabParameter : parameterList) {
        String key = andodabParameter.getKey();
        boolean isAlreadyIn = parameterMap.containsKey(key);
        if (!isAlreadyIn) {
          parameterMap.put(key, andodabParameter);
        }
      }
      id_parent = lazyAndodabObject.getParentId();
      Logger.getAnonymousLogger().info("id parent GET ALL = " + id_parent);
    } while (!id_parent.equals(getRootID()));


    return parameterMap;
  }

  /**
   * get not herited parameter of andodabobject by his id
   *
   * @param id
   * @return
   * @throws NotFoundElementException
   */
  public List<AndodabParameter> getParametersByAndodabObjectId(String id) throws NotFoundElementException {

    String parameterURL = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter/object/" + id;
    Uri uriToParse = Uri.parse(parameterURL);
    Cursor paramCursor = this.contentResolver.query(uriToParse, null, null, null, null);

    List<AndodabParameter> parameterList = new ArrayList<>();
    if (!paramCursor.moveToFirst()) {
      paramCursor.close();
      return parameterList;
    } else {
      do {

        String id_param = paramCursor.getString(paramCursor.getColumnIndex("id_obj"));
        String key = paramCursor.getString(paramCursor.getColumnIndex("key"));
        String typeOfSlaveParameter = paramCursor.getString(paramCursor.getColumnIndex("typeOfSlaveParameter"));
        String id_slave = paramCursor.getString(paramCursor.getColumnIndex("id_slave"));
        LazyAndodabObject lazyAndodabObjectSlave = getLazyAndodabObjectById(id_slave);
        String id_master = paramCursor.getString(paramCursor.getColumnIndex("id_master"));
        LazyAndodabObject lazyAndodabObjectMaster = getLazyAndodabObjectById(id_master);
        AndodabParameter andodabParameter = new AndodabParameter(id_param, key, lazyAndodabObjectSlave, AndodabParameter.TypeOfSlaveParameter.valueOf(typeOfSlaveParameter), lazyAndodabObjectMaster);
        parameterList.add(andodabParameter);
      } while (paramCursor.moveToNext());
    }
    paramCursor.close();
    return parameterList;
  }

  public AndodabParameter getAndodabParameterById(String id) throws NotFoundElementException {

    String parameterURL = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter/" + id;
    Uri uriToParse = Uri.parse(parameterURL);
    Cursor paramCursor = this.contentResolver.query(uriToParse, null, null, null, null);


    if (!paramCursor.moveToFirst()) {
      paramCursor.close();
      return null;
    } else {


      String id_param = paramCursor.getString(paramCursor.getColumnIndex("id_obj"));
      String key = paramCursor.getString(paramCursor.getColumnIndex("key"));
      String typeOfSlaveParameter = paramCursor.getString(paramCursor.getColumnIndex("typeOfSlaveParameter"));
      String id_slave = paramCursor.getString(paramCursor.getColumnIndex("id_slave"));
      LazyAndodabObject lazyAndodabObjectSlave = getLazyAndodabObjectById(id_slave);
      String id_master = paramCursor.getString(paramCursor.getColumnIndex("id_master"));
      LazyAndodabObject lazyAndodabObjectMaster = getLazyAndodabObjectById(id_master);
      paramCursor.close();
      return new AndodabParameter(id_param, key, lazyAndodabObjectSlave, AndodabParameter.TypeOfSlaveParameter.valueOf(typeOfSlaveParameter), lazyAndodabObjectMaster);


    }

  }

  /**
   * @param id
   * @return
   * @throws NotFoundElementException
   */
  public LazyAndodabObject getLazyAndodabObjectById(String id) throws NotFoundElementException {
    String andodabURL = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/lazy/" + id;

    Uri uriToParse = Uri.parse(andodabURL);
    Cursor c = this.contentResolver.query(uriToParse, null, null, null,
      "name");
    // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
    if (!c.moveToFirst()) {
      throw new NotFoundElementException();
    } else {
      String id_obj = c.getString(c.getColumnIndex("id_obj"));
      String name = c.getString(c.getColumnIndex("name"));
      String strType = c.getString(c.getColumnIndex("type"));
      int intSeal = c.getInt(c.getColumnIndex("seal"));
      String parent_Id = c.getString(c.getColumnIndex("parent_id"));
      boolean isSeal = intSeal == 1;
      LazyAndodabObject lazyAndodabObject = new LazyAndodabObject(id_obj, name, ObjectType.valueOf(strType), parent_Id, isSeal);
      c.close();
      return lazyAndodabObject;
    }

  }

  /**
   * this method will return all children of a parent object identify by his ID
   *
   * @param parentID
   * @return
   */
  public List<LazyAndodabObject> getLazyAndodabObjectChildrenByParentId(String parentID) {
    {
      String url = AndodabContentProvider.URL
        + AndodabContentProvider.BASE_PATH + "/lazy/parent/" + parentID;

      Uri uriToParse = Uri.parse(url);
      Cursor c = this.contentResolver.query(uriToParse, null, null, null, null);

      List<LazyAndodabObject> lazyAndoList = new ArrayList<LazyAndodabObject>();

      if (!c.moveToFirst()) {
        c.close();
        return lazyAndoList;
      } else {
        do {
          String id_obj = c.getString(c.getColumnIndex("id_obj"));
          if (!id_obj.equals(parentID)) {
            String name = c.getString(c.getColumnIndex("name"));
            String strType = c.getString(c.getColumnIndex("type"));
            int intSeal = c.getInt(c.getColumnIndex("seal"));
            String parent_Id = c.getString(c.getColumnIndex("parent_id"));
            boolean isSeal = intSeal == 1;
            LazyAndodabObject lazyAndo = new LazyAndodabObject(id_obj, name, ObjectType.valueOf(strType), parent_Id, isSeal);
            lazyAndoList.add(lazyAndo);
          }
        } while (c.moveToNext());

      }
      c.close();
      return lazyAndoList;
    }
  }

  /**
   * insert an object in database corresponding to the given andodab object
   *
   * @param andodab
   * @throws DataBaseIntegrityException
   */
  public void insertAndodabObject(AndodabObject andodab)
    throws DataBaseIntegrityException, NotFoundElementException, IOException {

    //PHASE 1 : INSERT AN ANDODABOBJECT
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH;
    ContentValues andodabValues = new ContentValues();
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_ID, andodab.getId());
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_NAME, andodab.getName());
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_TYPE, andodab.getType().name());
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_SEAL, andodab.isSealed());
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_VAL, andodab.getValue());
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_PARENTID, andodab.getParentId());

    ObjectCreationEvent objectCreationEvent = new ObjectCreationEvent();
    objectCreationEvent.setEventTarget(andodab);
    objectCreationEvent.setTime(System.nanoTime());
    if (eventRecorder != null)
      eventRecorder.add(objectCreationEvent);

        Uri uriToParse = Uri.parse(url);
        Uri uri = this.contentResolver.insert(uriToParse, andodabValues);
        Logger.getAnonymousLogger().warning(uri.toString());
        //  insertAndodabParameter(new AndodabParameter("orange", getLazyAndodabObjectById(new Long(7)), AndodabParameter.TypeOfSlaveParameter.DEFINE_OBJECT, getLazyAndodabObjectById(new Long(6))));

    // VALUES (2,'fruit','DEFINE_OBJECT',6,4)");
//           insertAndodabParameter(new AndodabParameter("fruit", getLazyAndodabObjectById(new Long(4)), AndodabParameter.TypeOfSlaveParameter.DEFINE_OBJECT, getLazyAndodabObjectById(new Long(6))));

  }

  /**
   * insertion de parametre
   *
   * @param andodabParameter
   */
  public void insertAndodabParameter(AndodabParameter andodabParameter) throws IOException {
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter";

//        ContentValues andodabParameterValues = new ContentValues();
    String slaveId = andodabParameter.getSlave().getId();
    String key = andodabParameter.getKey();
    String idMaster = andodabParameter.getMaster().getId();
    String name = andodabParameter.getType().name();

    ContentValues andodabValues = new ContentValues();
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_KEY, key);
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_ID_MASTER, idMaster);
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_ID_SLAVE, slaveId);
    andodabValues.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_TYPE, name);

    ParameterCreationEvent parameterCreationEvent = new ParameterCreationEvent();
    parameterCreationEvent.setParameter(andodabParameter);
    parameterCreationEvent.setTime(System.nanoTime());
    if (eventRecorder != null)
      eventRecorder.add(parameterCreationEvent);

    Uri uriToParse = Uri.parse(url);
    Uri uri = this.contentResolver.insert(uriToParse, andodabValues);

  }

  /**
   * je recupere le parametre de l'ando par id et ayant la clé key
   *
   * @param id
   * @param key
   * @return
   * @throws NotFoundElementException
   */
  public AndodabParameter getAndodabParameterByKeyAndIdMaster(String id, String key) throws NotFoundElementException {
    Map<String, AndodabParameter> allParameterMap = getAllParameterMap(id, new ArrayList<AndodabParameter>());
    return allParameterMap.get(key);
  }

  /**
   * update an object in database corresponding to the given andodab object
   *
   * @param info
   * @throws NotFoundElementException
   * @throws DataBaseIntegrityException
   */
  public int updateAndodabObject(AndodabObject info)
    throws NotFoundElementException, DataBaseIntegrityException, IOException {

    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/" + info.getId();

    ContentValues values = new ContentValues();
    values.put(AndodabContentProvider.TABLE_COLUMN_ID, info.getId());
    values.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_NAME, info.getName());
    values.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_TYPE, info.getType().name());
    values.put(AndodabContentProvider.TABLE_COLUMN_OBJECT_PARENTID, info.getParentId());
    values.put(AndodabContentProvider.TABLE_COLUMN_VAL, info.getValue());

    ObjectUpdateEvent objectUpdateEvent = new ObjectUpdateEvent();
    objectUpdateEvent.setEventTarget(info);
    objectUpdateEvent.setTime(System.nanoTime());
    if (eventRecorder != null)
      eventRecorder.add(objectUpdateEvent);

    String mSelectionClause = AndodabContentProvider.TABLE_COLUMN_ID + "=?";
    String[] mSelectionArgs = {info.getId() + ""};

    Uri uriToParse = Uri.parse(url);
    int result = this.contentResolver.update(uriToParse, values, mSelectionClause, mSelectionArgs);
    Logger.getAnonymousLogger().info("Result and Update = " + result);
    return result;

  }

  public int updateAndodabParameter(AndodabParameter info) throws IOException {
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter/" + info.getId();

    ContentValues values = new ContentValues();
    values.put(AndodabContentProvider.TABLE_COLUMN_ID, info.getId());
    values.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_KEY, info.getKey());
    values.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_ID_MASTER, info.getMaster().getId());
    values.put(AndodabContentProvider.TABLE_COLUMN_PARAMETER_ID_SLAVE, info.getSlave().getId());

    ParameterUpdateEvent parameterUpdateEvent = new ParameterUpdateEvent();
    parameterUpdateEvent.setParameter(info);
    parameterUpdateEvent.setTime(System.nanoTime());
    if (eventRecorder != null) {
      Logger.getAnonymousLogger().info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> got there b");
      eventRecorder.add(parameterUpdateEvent);
    } else {
      Logger.getAnonymousLogger().info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> got there c");
    }

    String mSelectionClause = AndodabContentProvider.TABLE_COLUMN_ID + "=?";
    String[] mSelectionArgs = {info.getId() + ""};

    Uri uriToParse = Uri.parse(url);
    int result = this.contentResolver.update(uriToParse, values, mSelectionClause, mSelectionArgs);
    Logger.getAnonymousLogger().info("Result param Update = " + result);
    return result;
  }

  /**
   * suppress element in the database corresponding to the given id if found
   *
   * @param id
   * @return
   * @throws NotFoundElementException
   */
  public int deleteAndodabObjectById(String id) throws IOException {

    // TENTATIVE SOLUTION AVEC CASCADE ON /////////////
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/" + id;

    ObjectDeletionEvent objectDeletionEvent = new ObjectDeletionEvent();
    AndodabObject andodabObject = new AndodabObject();
    andodabObject.setId(id);
    objectDeletionEvent.setEventTarget(andodabObject);
    objectDeletionEvent.setTime(System.nanoTime());
    if (eventRecorder != null)
      eventRecorder.add(objectDeletionEvent);

    Uri uriToParse = Uri.parse(url);
    int delete = this.contentResolver.delete(uriToParse, null, null);
    Logger.getAnonymousLogger().info("Delete result =" + delete);
    return delete;
  }

  public int deleteAndodabParameterById(String id) throws IOException {
    String url = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter/" + id;

    ParameterDeletionEvent parameterDeletionEvent = new ParameterDeletionEvent();
    AndodabParameter andodabParameter = new AndodabParameter();
    andodabParameter.setId(id);
    parameterDeletionEvent.setParameter(andodabParameter);
    parameterDeletionEvent.setTime(System.nanoTime());
    if (eventRecorder != null)
      eventRecorder.add(parameterDeletionEvent);;

    Uri uriToParse = Uri.parse(url);
    int delete = this.contentResolver.delete(uriToParse, null, null);
    Logger.getAnonymousLogger().info("Delete result =" + delete);
    return delete;
  }

  /**
   * je suis son fils si je retrouve son id dans mon ascendance
   *
   * @param parent
   * @param child
   * @return
   * @throws NotFoundElementException
   */
  public Boolean isAndoChildHeritedFromAndoParent(LazyAndodabObject parent, LazyAndodabObject child) throws NotFoundElementException {
    String id_Parent = child.getId();
    while (!id_Parent.equals(getRootID())) {
      String id_current = parent.getId();
      Logger.getAnonymousLogger().info("teste heritage : id_parent =" + parent.getId() + " =child_parent1= " + id_Parent);
      if (id_Parent.equals(id_current)) {
        return true;
      }

      LazyAndodabObject lazyAndodabObjectById = getLazyAndodabObjectById(id_Parent);
      id_Parent = lazyAndodabObjectById.getParentId();

    }
    return false;
  }

  public List<AndodabParameter> getAndodabParameterBySlaveId(String id) throws NotFoundElementException {
    String parameterURL = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/parameter/slave/" + id;
    Uri uriToParse = Uri.parse(parameterURL);
    Cursor paramCursor = this.contentResolver.query(uriToParse, null, null, null, null);

    List<AndodabParameter> parameterList = new ArrayList<>();
    if (!paramCursor.moveToFirst()) {
      paramCursor.close();
      return parameterList;
    } else {
      do {

        String id_param = paramCursor.getString(paramCursor.getColumnIndex("id_obj"));
        String key = paramCursor.getString(paramCursor.getColumnIndex("key"));
        String typeOfSlaveParameter = paramCursor.getString(paramCursor.getColumnIndex("typeOfSlaveParameter"));
        String id_slave = paramCursor.getString(paramCursor.getColumnIndex("id_slave"));
        String id_master = paramCursor.getString(paramCursor.getColumnIndex("id_master"));
        AndodabParameter andodabParameter = new AndodabParameter(id_param, key, getLazyAndodabObjectById(id_slave), AndodabParameter.TypeOfSlaveParameter.valueOf(typeOfSlaveParameter), getLazyAndodabObjectById(id_master));
        parameterList.add(andodabParameter);
      } while (paramCursor.moveToNext());
    }
    paramCursor.close();
    return parameterList;
  }

  /**
   * test si l'andodab dont lid est parent est l'ancetre de child
   *
   * @param parent
   * @param child
   * @return
   * @throws NotFoundElementException
   */
  public boolean isAndoChildHeritedFromAndoParent(String parent, String id, String child) throws NotFoundElementException {

    String id_current = child;
    Logger.getAnonymousLogger().info("inside 1 = " + id_current);
    while (!id_current.equals(getRootID())) {
      if (id.equals(id_current)) {
        return false;
      }
      Logger.getAnonymousLogger().info("teste heritage : id_parent =" + parent + " =child_parent1= " + id_current);
      if (id_current.equals(parent)) {
        return true;
      }
      LazyAndodabObject lazyAndodabObjectById = getLazyAndodabObjectById(id_current);
      id_current = lazyAndodabObjectById.getParentId();

    }
    return false;
  }

  public String getRootID() {
    String andodabURL = AndodabContentProvider.URL
      + AndodabContentProvider.BASE_PATH + "/root/";

        String[] projection = {AndodabContentProvider.TABLE_COLUMN_ID};
        Uri uriToParse = Uri.parse(andodabURL);
        Cursor c = this.contentResolver.query(uriToParse, projection, null, null,
                "name");
        // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
        if (!c.moveToFirst()) {
            c.close();
            throw new IllegalArgumentException("Il semblerait que cette base de donées n'est de root");
        } else {
            String id_obj = c.getString(c.getColumnIndex("id_obj"));
            c.close();
            return id_obj;
        }

    }

    public String getSTRINGID() {
        String andodabURL = AndodabContentProvider.URL
                + AndodabContentProvider.BASE_PATH + "/string/1";

        String[] projection = {AndodabContentProvider.TABLE_COLUMN_ID};
        Uri uriToParse = Uri.parse(andodabURL);
        Cursor c = this.contentResolver.query(uriToParse, projection, null, null,
                "name");
        // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
        if (!c.moveToFirst()) {
            c.close();
            throw new IllegalArgumentException("Il semblerait que cette base de donées n'est de string racine");
        } else {
            String id_obj = c.getString(c.getColumnIndex("id_obj"));
            c.close();
            return id_obj;
        }

    }

    public String getINTEGERID() {
        String andodabURL = AndodabContentProvider.URL
                + AndodabContentProvider.BASE_PATH + "/integer/1";

        String[] projection = {AndodabContentProvider.TABLE_COLUMN_ID};
        Uri uriToParse = Uri.parse(andodabURL);
        Cursor c = this.contentResolver.query(uriToParse, projection, null, null,
                "name");
        // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
        if (!c.moveToFirst()) {
            c.close();
            throw new IllegalArgumentException("Il semblerait que cette base de donées n'est d'integer racine");
        } else {
            String id_obj = c.getString(c.getColumnIndex("id_obj"));
            c.close();
            return id_obj;
        }

    }

    public String getFLOATID() {
        String andodabURL = AndodabContentProvider.URL
                + AndodabContentProvider.BASE_PATH + "/float/1";

        String[] projection = {AndodabContentProvider.TABLE_COLUMN_ID};
        Uri uriToParse = Uri.parse(andodabURL);
        Cursor c = this.contentResolver.query(uriToParse, projection, null, null,
                "name");

        // FIRST I GET ANDODABOBJECT WITHOUT PARAMETER
        if (!c.moveToFirst()) {
            c.close();
            throw new IllegalArgumentException("Il semblerait que cette base de donées n'est de float racine");
        } else {
            String id_obj = c.getString(c.getColumnIndex("id_obj"));

            c.close();
            return id_obj;
    }

  }
}
