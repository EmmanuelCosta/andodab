package fr.umlv.andodab.contentProvider.exception;

/**
 * throw when an operation will not respect the integrity of the database such
 * as updated a not sealed object into one if this object have child which
 * heritad of it
 * 
 * @author emmanuel
 *
 */
public class DataBaseIntegrityException extends Exception {
	public DataBaseIntegrityException() {

	}

	public DataBaseIntegrityException(String message) {
		super(message);
	}
}
