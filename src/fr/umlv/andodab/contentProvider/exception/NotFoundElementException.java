package fr.umlv.andodab.contentProvider.exception;

/**
 * this exception is throw when element can not be found in the database
 * @author emmanuel
 *
 */
public class NotFoundElementException extends Exception {

	public NotFoundElementException() {

	}

	public NotFoundElementException(String message) {
		super(message);
	}
}
