package fr.umlv.andodab.contentProvider;


/**
 * parameter of a andodab object
 *
 * @author emmanuel
 */
public class AndodabParameter {

    private String id;
    private String key;
    // the one who own this parameter
//    private LazyAndodabObject master;
    // the containing parameter
    private LazyAndodabObject slave;
    private LazyAndodabObject master;
    private TypeOfSlaveParameter typeOfSlaveParameter;
    private Boolean isInherited;

   /**
   * Default constructor.
   */
    public AndodabParameter() {
      this(null, null, null, null, null, null);
    }


    public enum TypeOfSlaveParameter {
        DEFINE_OBJECT, ABSTRACT_COMMON, ABSTRACT_FLOAT, ABSTRACT_INTEGER, ABSTRACT_STRING;
    }

    public AndodabParameter(String key,
                            LazyAndodabObject slave,TypeOfSlaveParameter typeOfSlaveParameter,LazyAndodabObject master) {

        this.key = key;
        this.slave = slave;
        this.typeOfSlaveParameter = typeOfSlaveParameter ;
        this.isInherited = false;
        if(master == null ){
            throw new IllegalArgumentException("master pas null");
        }
        this.master = master;
    }

    public AndodabParameter(String id, String key,
                            LazyAndodabObject slave,TypeOfSlaveParameter typeOfSlaveParameter,LazyAndodabObject master) {
        this.id = id;
        this.key = key;
        this.slave = slave;
        this.typeOfSlaveParameter = typeOfSlaveParameter ;
        this.isInherited = false;
        if(master == null ){
            throw new IllegalArgumentException("master pas null");
        }
        this.master = master;
    }
    public AndodabParameter(String id, String key,
                            LazyAndodabObject slave,TypeOfSlaveParameter typeOfSlaveParameter,LazyAndodabObject master,Boolean isInherited) {
        this.id = id;
        this.key = key;
        this.slave = slave;
        this.typeOfSlaveParameter = typeOfSlaveParameter ;
        this.isInherited = isInherited;
       if(master == null ){
           throw new IllegalArgumentException("master pas null");
       }
        this.master = master;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
      this.id = id;
    }

  public String getKey() {
        return key;
    }

//    public LazyAndodabObject getMaster() {
//        return master;
//    }

    public LazyAndodabObject getSlave() {
        return slave;
    }

    public LazyAndodabObject getMaster() {
        return master;
    }

    public TypeOfSlaveParameter getType() {
        return typeOfSlaveParameter;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setMaster(LazyAndodabObject master) {
        this.master = master;
    }

    public void setSlave(LazyAndodabObject slave) {
        this.slave = slave;
    }

    public void setTypeOfSlaveParameter(TypeOfSlaveParameter typeOfSlaveParameter) {
        this.typeOfSlaveParameter = typeOfSlaveParameter;
    }

    //this help to know if a parameter have a define value
    //or if it  just define an object which value will be define but an herited child
    // such as <energy  : Float>
    // here energy is the key and Float it 's an object without value
    // isSlaveParameterAbstract will return true
    //but if we have <energy : 100.0 >
    // it will return false because 100.0 is a define float_obj
    public boolean isSlaveParameterAbstract() {
        return !typeOfSlaveParameter.equals("DEFINE_OBJECT");
    }
}
