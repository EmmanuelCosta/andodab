package fr.umlv.andodab.contentProvider;

/**
 * sqlite request factory
 *
 * @author emmanuel
 */
public class SQLiteRequestFactory {

    public static String createTableObject() {
        return "CREATE TABLE object ( id_obj STRING PRIMARY KEY," +
                "name VARCHAR(100)  NOT NULL , "
                + "type TEXT(1) CHECK( type IN (\"R\",\"C\",\"F\",\"I\",\"S\") ) NOT NULL DEFAULT \"C\","
                + "seal BOOLEAN NOT NULL DEFAULT '1',"
                + "parent_id STRING NOT NULL REFERENCES object(id_obj)ON DELETE CASCADE)";
    }

    public static String createTableFloatObject() {
        return "CREATE TABLE float_obj (  id_obj STRING PRIMARY KEY,"
                + "name VARCHAR(100)  NOT NULL,"
                + "val  REAL,"
                + " type TEXT(1) CHECK( type IN (\"F\") ) NOT NULL DEFAULT \"F\","
                + "FOREIGN KEY(id_obj) REFERENCES object (id_obj)  ON DELETE CASCADE)";
    }

    public static String createTableIntegerObject() {
        return "CREATE TABLE integer_obj (  id_obj STRING PRIMARY KEY,"
                + " name VARCHAR(100)  NOT NULL,"
                + "val INTEGER,"
                + " type TEXT(1) CHECK( type IN (\"I\") ) NOT NULL DEFAULT \"I\","
                + "FOREIGN KEY(id_obj) REFERENCES object (id_obj)  ON DELETE CASCADE)";
    }

    public static String createTableStringObject() {
        return "CREATE TABLE string_obj (  id_obj STRING PRIMARY KEY,"
                + " name VARCHAR(100)  NOT NULL,"
                + "val TEXT,"
                + " type TEXT(1) CHECK( type IN (\"S\") ) NOT NULL DEFAULT \"S\","
                + "FOREIGN KEY(id_obj) REFERENCES object (id_obj)  ON DELETE CASCADE )";
    }

    public static String createTableCommonObject() {
        return "CREATE TABLE common_obj ( id_obj STRING PRIMARY KEY,"
                + "name VARCHAR(100)  NOT NULL,"
                + " type TEXT(1) CHECK( type IN (\"C\") ) NOT NULL DEFAULT \"C\","
                + "FOREIGN KEY(id_obj) REFERENCES object (id_obj)  ON DELETE CASCADE )";
    }

    public static String createTableParameterObject() {
        return "CREATE TABLE parameter_obj ( id_obj STRING PRIMARY KEY,"
                + "key TEXT  NOT NULL,"
                + "typeOfSlaveParameter TEXT CHECK( typeOfSlaveParameter IN (\"DEFINE_OBJECT\",\"ABSTRACT_COMMON\",\"ABSTRACT_FLOAT\",\"ABSTRACT_INTEGER\",\"ABSTRACT_STRING\") ) NOT NULL,"
                + "id_master STRING NOT NULL REFERENCES object(id_obj)ON DELETE CASCADE ,"
                + "id_slave STRING   REFERENCES object(id_obj)ON DELETE CASCADE )";
    }
}
