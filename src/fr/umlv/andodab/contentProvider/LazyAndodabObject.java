package fr.umlv.andodab.contentProvider;

import fr.umlv.andodab.contentProvider.AndodabObject.ObjectType;

/**
 * this represente a partial object with minimal information
 *
 * @author emmanuel
 */
public class LazyAndodabObject {
  private final String id;
  private String name;
  private String parentId;
  private ObjectType type;
  private Boolean isSealed;



  public LazyAndodabObject(String id) {
    this(id, null, null, null, null);
  }

  public LazyAndodabObject(String id, String name, ObjectType type,
                           String parentId, Boolean isSeal) {
    this.id = id;
    this.name = name;
    this.parentId = parentId;
    this.type = type;
    this.isSealed = isSeal;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getParentId() {
    return parentId;
  }

  public ObjectType getType() {
    return type;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(ObjectType type) {
    this.type = type;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public boolean isSealed() {
    return isSealed;
  }

  public void setSealed(boolean isSealed) {
    this.isSealed = isSealed;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    LazyAndodabObject other = (LazyAndodabObject) o;

    return id.equals(other.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return getName();
  }
}
