package fr.umlv.andodab.contentProvider;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import fr.umlv.andodab.contentProvider.exception.AndodabIllegalMethodException;

/**
 * this represente a basic object on the database
 * 
 * @author emmanuel
 *
 */
public class AndodabObject {
	private String id;
	private String name;
	private String parentId;
	private List<AndodabParameter> params;
	private  ObjectType type;
	private Boolean isSealed;
    private String value="NOT_DEFINE";


  /**
   * Default constructor.
   */
  public AndodabObject() {
    this(null, null, null, null, null, null);
  }

	public AndodabObject(String id, String name, ObjectType type,
                         String parentId, Boolean sealed) {

		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.params = new ArrayList<AndodabParameter>();
		this.type = type;
		this.isSealed = sealed;
	}

    public AndodabObject( String name, ObjectType type,
                          String parentId, Boolean sealed) {
        this.id =  new BigInteger(50, new Random()).toString();
        this.name = name;
        this.parentId = parentId;
        this.params = new ArrayList<AndodabParameter>();
        this.type = type;
        this.isSealed = sealed;
    }

    public AndodabObject(String id, String name, ObjectType type,
                         String parentId, Boolean sealed,String value) {

        this(id,name, type, parentId,sealed);
        this.value = value;
    }

    public AndodabObject( String name, ObjectType type,
                          String parentId, Boolean sealed,String value) {

        this(name, type, parentId,sealed);
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    /**
	 * this are all the accept type of object
	 * 
	 * @author emmanuel
	 *
	 */
	public enum ObjectType {
		R("Root"), C("Common"), F("Float"), I("Integer"), S("String");
		private String type;

		private ObjectType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	public String getId() {
		return id;
	}

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
		return name;
	}

	public String getParentId() {
		return parentId;
	}

	public List<AndodabParameter> getParams() {
		return params;
	}

	public ObjectType getType() {
		return type;
	}

  public void setType(ObjectType type) {
    this.type = type;
  }

  public Boolean isSealed() {
		return isSealed;
	}

	public void setName(String name) {
		this.name = name;
	}

    public void setValue(String value) {
        this.value = value;
    }

    public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setParams(List<AndodabParameter> params) {
		this.params = params;
	}

	public void setSealed(Boolean sealed) {
		this.isSealed = sealed;
	}

    public void addAndodabParameter(AndodabParameter andodabParameter) {
        this.params.add(andodabParameter);
    }

    public String getStringValue() throws AndodabIllegalMethodException {
        if(type.toString().equals("S")){
            return value;
        }
        throw new AndodabIllegalMethodException(String.format("Vous essayez de recuperer une valeur String hors cette objet à une valeur de type : %s", type.toString()));

    }
    public Float getFloatValue() throws AndodabIllegalMethodException {
        if(type.toString().equals("F")){
            return new Float(value);
        }
        throw new AndodabIllegalMethodException(String.format("Vous essayez de recuperer une valeur Float hors cette objet à une valeur de type : %s", type.toString()));
    }

    public Integer getIntegerValue() throws AndodabIllegalMethodException {
        if(type.toString().equals("I")){
            return new Integer(value);
        }
        throw new AndodabIllegalMethodException(String.format("Vous essayez de recuperer un valeur Integer or cette objet à une valeur de type : %s", type.toString()));
    }

    public boolean isValueDefine(){
        return value != null;
    }

    public AndodabParameter getAndoParameterByKey(String key){

        for(AndodabParameter andodabParameter : params){
            if(andodabParameter.getKey().equals(key)){
                return andodabParameter;
            }
        }
        return null;
    }

    public LazyAndodabObject toLazyAndodabObject(){
        return new LazyAndodabObject(id,name,type,parentId,isSealed);
    }
}
