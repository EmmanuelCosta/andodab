package fr.umlv.andodab.synchronization;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The event recorder class contains and maintains a list of the last events.
 * @author Mathieu Abou-Aichi.
 */
public class EventRecorder {
  private Map<Long, Event> events;
  private Context context;

  /**
   * Default constructor.
   */
  EventRecorder(Context context) {
    events = new HashMap<>();
    this.context = context;
  }

  /**
   * Gets a specific event.
   * @param time when the event occurred.
   * @return the corresponding event.
   * @throws java.lang.IllegalArgumentException if the index is invalid.
   */
  public Event getEvent(Long time) {
    if (time < 0) {
      throw new IllegalArgumentException(
        "The index must be greater than 0");
    }
    return events.get(time);
  }

  /**
   * Gets the last event.
   * @return the last event, or null if no event has been recorded.
   */
  public Event getLast() {
    Set<Long> times = events.keySet();
    if (events.isEmpty()) {
      return null;
    }
    Long max = -1l;
    for (Long time : times) {
      if (max < time) {
        max = time;
      }
    }
    return events.get(max);
  }

  /**
   * Adds an event to the list, provided it is not null.
   * @param event the new event to add.
   */
  public void add(Event event) throws IOException {
    if (event != null) {
      events.put(event.getTime(), event);
    }
    EventWriter.write(event, context);
  }

  /**
   * Gets the last events from a certain point in time.
   * @param time the starting time.
   * @return the last events.
   * @throws java.lang.IllegalArgumentException if the time is invalid.
   */
  public List<Event> getLastEvents(long time) {
    if (time < 0) {
      throw new IllegalArgumentException(
        "The index must be greater than 0, and lesser than the last index.");
    }
    if (events.isEmpty()) {
      return Collections.unmodifiableList(Collections.<Event>emptyList());
    }
    List<Long> timeList = getTimeList();
    int startingIndex = getStartingIndex(timeList, time);
    if (startingIndex == -1) {
      return Collections.unmodifiableList(Collections.<Event>emptyList());
    }
    return getLastEvents(startingIndex, timeList);
  }

  private List<Event> getLastEvents(int startingIndex, List<Long> timeList) {
    List<Event> lastEvents = new ArrayList<>();
    for (int i = startingIndex; i < events.size(); i++) {
      lastEvents.add(events.get(timeList.get(i)));
    }
    return lastEvents;
  }

  private List<Long> getTimeList() {
    Long[] times = new Long[events.size()];
    events.keySet().toArray(times);
    Arrays.sort(times);
    return Arrays.asList(times);
  }

  private int getStartingIndex(List<Long> timeList, Long time) {
    for (int i = 0; i < timeList.size(); i++) {
      if (timeList.get(i) > time) {
       return i;
      }
    }
    return -1;
  }
}
