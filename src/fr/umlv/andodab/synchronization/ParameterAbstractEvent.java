package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabParameter;

/**
 * Abstract event class for parameters.
 * @author Mathieu Abou-Aichi.
 */
public abstract class ParameterAbstractEvent extends AbsractEvent {
  private AndodabParameter parameter;

  /**
   * Default constructor.
   */
  ParameterAbstractEvent() {
    this(null, null);
  }

  /**
   * Alternative constructor.
   * @param parameter the parameter
   */
  ParameterAbstractEvent(AndodabParameter parameter, Long time) {
    super(time);
    this.parameter = parameter;
  }

  /**
   * Gets the parameter.
   * @return the parameter.
   */
  public AndodabParameter getParameter() {
    return parameter;
  }

  /**
   * Sets the parameter.
   * @param parameter the parameter.
   */
  public void setParameter(AndodabParameter parameter) {
    this.parameter = parameter;
  }

  @Override
  public ObjectType getObjectType() {
    return ObjectType.PARAMETER;
  }
}
