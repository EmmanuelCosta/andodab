package fr.umlv.andodab.synchronization;

/**
 * The abstract event class.
 * @author Mathieu Abou-Aichi.
 */
public abstract class AbsractEvent implements Event {
  private Long time;

  /**
   * Default constructor.
   */
  AbsractEvent() {
    this(null);
  }

  /**
   * Alternative constructor.
   * @param time the time the event occurred.
   */
  AbsractEvent(Long time) {
    this.time = time;
  }

  /**
   * Gets the stringified version of this object's subclasses.
   * @return the object's string value.
   */
  public abstract String getStringifiedValue();

  /**
   * Gets this object's type.
   * @return the object's type.
   */
  @Override
  public abstract ObjectType getObjectType();


  /**
   * Gets the event's time.
   * @return the event's time.
   */
  @Override
  public Long getTime() {
    return time;
  }

  /**
   * Sets the event's time.
   * @param time the event's time.
   */
  public void setTime(Long time) {
    this.time = time;
  }

  @Override
  public String toString() {
    return getTime() + ";" +
      getType() + ";" +
      getObjectType() + ";"
      + getStringifiedValue();
  }

  @Override
  public boolean equals(Object o) {
    if (null == o) return false;
    if (!(o instanceof AbsractEvent)) return false;

    AbsractEvent that = (AbsractEvent) o;

    if (time != null ? !time.equals(that.time) : that.time != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return time != null ? time.hashCode() : 0;
  }
}
