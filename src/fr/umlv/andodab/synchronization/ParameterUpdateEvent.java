package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabParameter;

/**
 * Created by Mathieu on 13/03/2015.
 */
public class ParameterUpdateEvent extends ParameterAbstractEvent {
  private static final long serialVersionUID = 6318859972033983516L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ParameterUpdateEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param parameter the parameter to update.
   * @param time the time the event occurred.
   */
  public ParameterUpdateEvent(AndodabParameter parameter, Long time) {
    super(parameter, time);
  }

  @Override
  public String getStringifiedValue() {
    return getParameter().getId() + ";"
      + getParameter().getKey() + ";"
      + getParameter().getMaster().getId() + ";"
      + getParameter().getSlave().getId() + ";";
  }

  @Override
  public EventType getType() {
    return EventType.UPDATE;
  }
}
