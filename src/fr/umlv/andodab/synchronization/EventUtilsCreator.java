package fr.umlv.andodab.synchronization;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * The Event utils creator class.
 * @author Mathieu Abou-Aichi.
 */
public final class EventUtilsCreator {
  private static EventRecorder eventRecorder = null; // late binding.
  private static Updater updater = null; // late binding.

  private EventUtilsCreator() {
  }

  /**
   * Gets the event recorder, initializes it if need be.<br/>
   * If the event recorder has already been created, the same instance will be returned.
   * @param context the context.
   * @return the event recorder.
   */
  public static EventRecorder getEventRecorder(Context context) throws IOException {
    if (eventRecorder == null) {
      eventRecorder = new EventRecorder(context);
    }
    List<Event> events = AndodabObjectsBuilder.createEvents(EventWriter.FILENAME, context);
    for (Event event : events) {
      eventRecorder.add(event);
    }
    return eventRecorder;
  }

  /**
   * Gets the updater.
   * @return the updater.
   */
  public static Updater getUpdater() {
    if (updater == null) {
      updater = new Updater();
    }
    return updater;
  }
}
