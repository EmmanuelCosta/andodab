package fr.umlv.andodab.synchronization;

import java.io.IOException;

import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.exception.DataBaseIntegrityException;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;

/**
 * The updater class. Updates the database for each event.<br/>
 * Runs in background and updates the database whenever an event occurs.
 * @author Mathieu Abou-Aichi.
 */
public class Updater {
  private AndodabDAO andodabDAO;
  private Long lastUpdate;

  /**
   * Default constructor.
   */
  Updater() {
  }

  /**
   * Alternative constructor.
   * @param andodabDAO the Andodab DAO.
   */
  Updater(AndodabDAO andodabDAO) {
    this.andodabDAO = andodabDAO;
  }

  /**
   * Updates this observer with a new event (either a creation, an update or a deletion).
   * @param event the occurring event.
   */
  public void update(Long updateTime, Event event)
    throws NotFoundElementException, DataBaseIntegrityException, IOException {
    switch (event.getType()) {
      case CREATION:
        if (event.getObjectType().equals(ObjectType.OBJECT)) {
          andodabDAO.insertAndodabObject(((ObjectCreationEvent) event).getEventTarget());
        } else {
          andodabDAO.insertAndodabParameter(((ParameterCreationEvent) event).getParameter());
        }
        break;
      case UPDATE:
        if (event.getObjectType().equals(ObjectType.OBJECT)) {
          andodabDAO.updateAndodabObject(((ObjectUpdateEvent) event).getEventTarget());
        } else {
          andodabDAO.updateAndodabParameter(((ParameterUpdateEvent) event).getParameter());
        }
        break;
      default: //deletion
        if (event.getObjectType().equals(ObjectType.OBJECT)) {
          andodabDAO.deleteAndodabObjectById(((ObjectDeletionEvent) event).getEventTarget().getId());
        } else {
          andodabDAO.deleteAndodabParameterById(((ParameterDeletionEvent) event).getParameter().getId());
        }
    }
    lastUpdate = updateTime;
  }

  /**
   * Gets the application DAO.
   * @return the DAO.
   */
  public AndodabDAO getAndodabDAO() {
    return andodabDAO;
  }

  /**
   * Sets the application DAO.
   * @param andodabDAO the DAO.
   */
  public void setAndodabDAO(AndodabDAO andodabDAO) {
    this.andodabDAO = andodabDAO;
  }

  public Long getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Long lastUpdate) {
    this.lastUpdate = lastUpdate;
  }
}
