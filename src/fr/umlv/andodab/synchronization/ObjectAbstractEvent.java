package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabObject;

/**
 * Abstract event class.
 * @author Mathieu Abou-Aichi.
 */
public abstract class ObjectAbstractEvent extends AbsractEvent {
  private AndodabObject eventTarget;

  /**
   * Default constructor.
   */
  ObjectAbstractEvent() {
    this(null, null);
  }

  /**
   * Constructor using a passed object as this event's target.
   * @param eventTarget the object.
   * @param time the time the event occurred.
   */
  ObjectAbstractEvent(AndodabObject eventTarget, Long time) {
    super(time);
    this.eventTarget = eventTarget;
  }

  @Override
  public ObjectType getObjectType() {
    return ObjectType.OBJECT;
  }

  /**
   * Gets the event's target.
   * @return the event's target.
   */
  public AndodabObject getEventTarget() {
    return eventTarget;
  }

  /**
   * Sets the event's target.
   * @param eventTarget the event's target.
   */
  public void setEventTarget(AndodabObject eventTarget) {
    this.eventTarget = eventTarget;
  }

}
