package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabObject;

import static fr.umlv.andodab.synchronization.EventType.CREATION;

/**
 * The object creation event class.
 * @author Mathieu Abou-Aichi.
 */
public class ObjectCreationEvent extends ObjectAbstractEvent {

  /**
   * Serial version UID.
   */
  private static final long serialVersionUID = -1689051710546373484L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ObjectCreationEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param object the object to create.
   * @param time the time the event occurred.
   */
  public ObjectCreationEvent(AndodabObject object, Long time) {
    super(object, time);
  }

  @Override
  public EventType getType() {
    return CREATION;
  }

  @Override
  public String getStringifiedValue() {
    return getEventTarget().getId() + ";"
      + getEventTarget().getName() + ";"
      + getEventTarget().getType() + ";"
      + getEventTarget().isSealed() + ";"
      + getEventTarget().getValue() + ";"
      + getEventTarget().getParentId();
  }
}
