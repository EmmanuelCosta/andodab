package fr.umlv.andodab.synchronization;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;

import fr.umlv.andodab.R;
import fr.umlv.andodab.contentProvider.exception.DataBaseIntegrityException;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;


public class ServerSynchroActivity extends Activity {
  TextView info, infoip, msg;
  String message = "";
  ServerSocket serverSocket;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_server_synchro);
    info = (TextView) findViewById(R.id.info);
    infoip = (TextView) findViewById(R.id.infoip);
    msg = (TextView) findViewById(R.id.msg);

    String infoipText=getIpAddress();
    if(infoipText.equals("")) {
      infoip.setText("Something Wrong! You aren't connect to a network.");
    }
    else {
      infoip.setText(infoipText);
      Thread socketServerThread = new Thread(new SocketServerThread());
      socketServerThread.start();
    }
  }

  private String getIpAddress() {
    String ip = "";
    try {
      Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces();
      while (enumNetworkInterfaces.hasMoreElements()) {
        NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
        Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
        while (enumInetAddress.hasMoreElements()) {
          InetAddress inetAddress = enumInetAddress.nextElement();
          if (inetAddress.isSiteLocalAddress())
            ip += "Site local address: " + inetAddress.getHostAddress() + "\n";
        }
      }
    } catch (SocketException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      ip += "Something Wrong! " + e.toString() + "\n";
    }
    return ip;
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (serverSocket != null) {
      try {
        serverSocket.close();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  // Serveur de connexion qui s'occupe de lancer dans un thread le protocol de synchronization
  private class SocketServerThread extends Thread {
    static final int SocketServerPORT = 6969;
    @Override
    public void run() {
      try {
        serverSocket = new ServerSocket(SocketServerPORT);
        ServerSynchroActivity.this.runOnUiThread(new Runnable() {
          @Override
          public void run() {
            msg.setText("Your device listens to this port: "
              + serverSocket.getLocalPort());
          }
        });

        while (true) {
          Socket socket = serverSocket.accept();
          message += "Connection for synchronization from " + socket.getInetAddress()+ ":" + socket.getPort() + "\n";
          ServerSynchroActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              msg.setText(message);
            }
          });
          SocketServerReplyTask socketServerReplyThread = new SocketServerReplyTask(socket);
          socketServerReplyThread.execute();
        }
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  // Thread qui s'occupe du protocole de synchronisation
  private class SocketServerReplyTask extends AsyncTask<Void,Void,Void> {
    private Socket hostThreadSocket;

    SocketServerReplyTask(Socket socket) {
      hostThreadSocket = socket;
    }

    @Override
    protected Void doInBackground(Void... params) {
      OutputStream out;InputStream in;
      try {
        //Ouverture des flux in et out pour communiquer entre les 2 applications
        out = hostThreadSocket.getOutputStream();
        in = hostThreadSocket.getInputStream();
        synchro(in,out);
        message += "End of synchrozisation with " + hostThreadSocket.getInetAddress()+ ":" + hostThreadSocket.getPort()+"\n";
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        message += "Something wrong! " + e.toString() + "\n";
      }finally {
        if(hostThreadSocket != null){
          try {
            hostThreadSocket.close();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      msg.setText(message);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
      super.onProgressUpdate(values);
      msg.setText(message);
    }

    private void synchro(InputStream in,OutputStream out) throws IOException {
      FileInputStream fin = null;
      BufferedInputStream bfin = null;
      try {
        byte [] byteArray  = new byte [1024];
        fin = getApplicationContext().openFileInput(AndodabObjectsBuilder.getLogFile().getAbsolutePath());
        bfin = new BufferedInputStream(fin);
        int count=0;
        while ((count = bfin.read(byteArray)) >= 0) {
          out.write(byteArray,0,count);
          out.flush();
          message+=new String(byteArray);
        }
      }
      catch (IOException ioe) {
      }finally {
        try {
          if(fin!=null)
            fin.close();
          if(bfin!=null)
            bfin.close();
        }catch(IOException ioe){}
      }


      message+="Transfer logs from server :\n";
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
      FileOutputStream ftmpout=null;
      File tmpFile=null;
      byte[] buffer = new byte[1024];
      int bytesRead;
      try {
        File logFile = AndodabObjectsBuilder.createLogFile();
        ftmpout=new FileOutputStream(logFile);
        while ((bytesRead = in.read(buffer)) != -1){
          byteArrayOutputStream.write(buffer, 0, bytesRead);
          message += byteArrayOutputStream.toString();
          ftmpout.write(buffer,0,bytesRead);
        }
      } catch (Exception e) {
        message+="Error during the reception of the logs\n";
        return;
      }finally {
        if(ftmpout!=null)
          try {
            ftmpout.close();
          } catch (IOException e) { }
      }

      message+="Synchronization from the log recept";
      updateDatabase();
      message+="Synchronization done";
    }
  }

  private void updateDatabase() throws IOException {
    List<Event> events =
      AndodabObjectsBuilder.createEvents(
        AndodabObjectsBuilder.getLogFile().getAbsolutePath(),
        getApplicationContext());
    for (Event event : events) {
      Updater updater = EventUtilsCreator.getUpdater();
      try {
        updater.update(System.nanoTime(), event);
      } catch (NotFoundElementException e) {
        e.printStackTrace();
      } catch (DataBaseIntegrityException e) {
        e.printStackTrace();
      }
    }
  }
}
