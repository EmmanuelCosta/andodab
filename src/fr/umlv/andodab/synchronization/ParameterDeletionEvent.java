package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabParameter;

/**
 * Created by Mathieu on 13/03/2015.
 */
public class ParameterDeletionEvent extends ParameterAbstractEvent {
  private static final long serialVersionUID = 5325666932434314963L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ParameterDeletionEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param parameter the parameter to delete.
   * @param time the time the event occurred.
   */
  public ParameterDeletionEvent(AndodabParameter parameter, Long time) {
    super(parameter, time);
  }

  @Override
  public String getStringifiedValue() {
    return getParameter().getId().toString();
  }

  @Override
  public EventType getType() {
    return EventType.DELETION;
  }
}
