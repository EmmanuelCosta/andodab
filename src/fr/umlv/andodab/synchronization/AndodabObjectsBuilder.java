package fr.umlv.andodab.synchronization;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.umlv.andodab.contentProvider.AndodabObject;
import fr.umlv.andodab.contentProvider.AndodabParameter;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;

/**
 * @author Mathieu Abou-Aichi.
 */
public final class AndodabObjectsBuilder {
  private AndodabObjectsBuilder() {
  }

  public static List<Event> createEvents(String fileName, Context context) throws IOException {
    List<Event> events = new ArrayList<>();

    File file = getLogFile();
    if (!file.exists()) {
      file = createLogFile();
//      return events;
    }
    Scanner scanner = new Scanner(file);
    while(scanner.hasNext()) {
      String line = scanner.next();
      String[] results = extractResults(line);
      ObjectAbstractEvent objectEvent = null;
      ParameterAbstractEvent parameterEvent = null;
      switch(results[2]) {
        case "OBJECT":
          switch (results[1]) {
            case "CREATION":
              objectEvent = new ObjectCreationEvent();
              break;
            case "UPDATE":
              objectEvent = new ObjectUpdateEvent();
              break;
            default: // DELETION
              objectEvent = new ObjectDeletionEvent();
          }
          objectEvent.setTime(Long.valueOf(results[0]));
          objectEvent.setEventTarget(createAndodabObject(line));
          break;
        default: // PARAMETER
          switch (results[1]) {
            case "CREATION":
              parameterEvent = new ParameterCreationEvent();
              break;
            case "UPDATE":
              parameterEvent = new ParameterUpdateEvent();
              break;
            default: // DELETION
              parameterEvent = new ParameterDeletionEvent();
          }
          parameterEvent.setTime(Long.valueOf(results[0]));
          parameterEvent.setParameter(createAndodabParameter(line));
      }
    }
    scanner.close();
    return events;
  }

  private static Boolean isSDCardOn() {
    switch (Environment.getExternalStorageState()) {
      case "MEDIA_MOUNTED":
        return true;
      default:
        return false;
    }
  }

  public static File getLogFile() throws IOException {
    if (!isSDCardOn()) {
      return File.createTempFile("logs", "txt");
    }
    File sdCard = Environment.getExternalStorageDirectory();
    File dir = new File (sdCard.getAbsolutePath() + "/andodab/");
    return new File(dir, EventWriter.FILENAME);
  }

  public static File createLogFile() throws IOException {
    if (!isSDCardOn()) {
      return File.createTempFile("logs", "txt");
    }
    File sdCard = Environment.getExternalStorageDirectory();
    File dir = new File (sdCard.getAbsolutePath() + "/andodab/");
    dir.mkdirs();
    File file = new File(dir, EventWriter.FILENAME);
    file.createNewFile();
    return file;
  }

  public static AndodabObject createAndodabObject(String string) {
    AndodabObject andodabObject = new AndodabObject();
    String[] results = extractResults(string);
    andodabObject.setId(results[3]);
    andodabObject.setName(results[4]);
    andodabObject.setType(AndodabObject.ObjectType.valueOf(results[5]));
    andodabObject.setSealed(Boolean.valueOf(results[6]));
    andodabObject.setValue(results[7]);
    andodabObject.setParentId(results[8]);
    return andodabObject;
  }

  public static AndodabParameter createAndodabParameter(String string) {
    AndodabParameter parameter = new AndodabParameter();
    String[] results = extractResults(string);
    parameter.setId(results[3]);
    parameter.setKey(results[4]);
    parameter.setSlave(new LazyAndodabObject(results[4]));
    parameter.setMaster(new LazyAndodabObject(results[5]));
    parameter.setTypeOfSlaveParameter(AndodabParameter.TypeOfSlaveParameter.valueOf(results[6]));
    return parameter;
  }

  private static String[] extractResults(String string) {
    return string.split(";");
  }
}
