package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabObject;

import static fr.umlv.andodab.synchronization.EventType.DELETION;

/**
 * The object deletion event class.
 * @author Mathieu Abou-Aichi.
 */
public class ObjectDeletionEvent extends ObjectAbstractEvent {
  /**
   * Serial version UID.
   */
  private static final long serialVersionUID = 5254543699431879339L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ObjectDeletionEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param object the object to delete.
   * @param time the time the event occurred.
   */
  public ObjectDeletionEvent(AndodabObject object, Long time) {
    super(object, time);
  }

  @Override
  public EventType getType() {
    return DELETION;
  }

  @Override
  public String getStringifiedValue() {
    return getEventTarget().getId().toString();
  }
}
