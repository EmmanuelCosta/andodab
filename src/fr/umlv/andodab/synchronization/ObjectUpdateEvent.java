package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabObject;

import static fr.umlv.andodab.synchronization.EventType.UPDATE;

/**
 * The object update event class.
 * @author Mathieu Abou-Aichi.
 */
public class ObjectUpdateEvent extends ObjectAbstractEvent {

  /**
   * Serial version UID.
   */
  private static final long serialVersionUID = -4218407884401402422L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ObjectUpdateEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param object the object to update.
   * @param time the time the event occurred.
   */
  public ObjectUpdateEvent(AndodabObject object, Long time) {
    super(object, time);
  }

  @Override
  public EventType getType() {
    return UPDATE;
  }

  @Override
  public String getStringifiedValue() {
    return getEventTarget().getId() + ";"
      + getEventTarget().getName() + ";"
      + getEventTarget().getType() + ";"
      + getEventTarget().isSealed() + ";"
      + getEventTarget().getParentId();
  }
}
