package fr.umlv.andodab.synchronization;

/**
 * Event type enum.
 * @author Mathieu Abou-Aichi.
 */
enum EventType {
  /** Creation event. */
  CREATION,
  /** Update event. */
  UPDATE,
  /** Deletion event. */
  DELETION
}
