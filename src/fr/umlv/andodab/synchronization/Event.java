package fr.umlv.andodab.synchronization;

import java.io.Serializable;

/**
 * The event interface.
 * @author Mathieu Abou-Aichi.
 */
public interface Event extends Serializable {
  /**
   * Gets the event's type (either a creation, an update or a deletion).
   * @return the event's type.
   */
  EventType getType();

  /**
   * Gets the event's time.
   * @return the time the event occurred.
   */
  Long getTime();

  /**
   * Gets the object's type.
   * @return the object's type.
   */
  ObjectType getObjectType();
}
