package fr.umlv.andodab.synchronization;

/**
 * This enum contains the various object types this applications possesses.
 * @author Mathieu Abou-Aichi.
 */
enum ObjectType {
  /** The AndodabObject type. */
  OBJECT,

  /** The AndodabParameter type.  */
  PARAMETER
}
