package fr.umlv.andodab.synchronization;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import fr.umlv.andodab.R;


public class ClientSynchroActivity extends Activity {
  TextView textResponse;
  EditText editTextAddress, editTextPort;
  Button buttonConnect;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_client_synchro);
    editTextAddress = (EditText) findViewById(R.id.address);
    editTextPort = (EditText) findViewById(R.id.port);
    buttonConnect = (Button) findViewById(R.id.connect);
    textResponse = (TextView) findViewById(R.id.response);

    buttonConnect.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        try {
          String address = editTextAddress.getText().toString();
          int port = Integer.parseInt(editTextPort.getText().toString());
          ClientTask myClientTask = new ClientTask(address, port);
          myClientTask.execute();
        }catch(Exception e){
          textResponse.setText("Bad arguments provided !");
        }
      }
    });
  }

  public class ClientTask extends AsyncTask<Void, Void, Void> {
    String dstAddress;
    int dstPort;
    String response = "";

    ClientTask(String addr, int port){
      dstAddress = addr;
      dstPort = port;
    }

    @Override
    protected Void doInBackground(Void... params) {
      Socket socket = null;
      try {
        socket = new Socket(dstAddress, dstPort);

        InputStream in = socket.getInputStream();
        OutputStream out=socket.getOutputStream();

        synchro(in,out);

      } catch (UnknownHostException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        response = "UnknownHostException: " + e.toString();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        response = "IOException: " + e.toString();
      }finally{
        if(socket != null){
          try {
            socket.close();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
      return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
      super.onProgressUpdate(values);
      textResponse.setText(response);
    }

    @Override
    protected void onPostExecute(Void result) {
      super.onPostExecute(result);
      textResponse.setText(response);
    }

    private void synchro(InputStream in, OutputStream out) throws IOException {
      //Lit les logs envoyer par le serveur
      response+="Transfer logs from server :\n";
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
      FileOutputStream ftmpout=null;
      File logFile=null;
      byte[] buffer = new byte[1024];
      int bytesRead;
      try {
        logFile = AndodabObjectsBuilder.getLogFile();
        ftmpout=new FileOutputStream(logFile);
        while ((bytesRead = in.read(buffer)) != -1){
          byteArrayOutputStream.write(buffer, 0, bytesRead);
          response += byteArrayOutputStream.toString();
          ftmpout.write(buffer,0,bytesRead);
        }
      } catch (Exception e) {
        response+="Error during the reception of the logs\n";
        return;
      }finally {
        if(ftmpout!=null)
          try {
            ftmpout.close();
          } catch (IOException e) { }
      }

      //Envoi les logs au serveur
      response+="\nSend logs to server:\n";
      FileInputStream fin = null;
      BufferedInputStream bfin = null;
      try {
        byte [] byteArray  = new byte [1024];
        fin = getApplicationContext().openFileInput(EventWriter.FILENAME);
        bfin = new BufferedInputStream(fin);
        int count=0;
        while ((count = bfin.read(byteArray)) >= 0) {
          out.write(byteArray,0,count);
          out.flush();
          response+=new String(byteArray);
        }
      }
      catch (IOException ioe) {
      }finally {
        try {
          if(fin!=null)
            fin.close();
          if(bfin!=null)
            bfin.close();
        }catch(IOException ioe){}
      }

      response+="Synchronization from the log recept";
      response+="Synchronization done";
    }
  }
}
