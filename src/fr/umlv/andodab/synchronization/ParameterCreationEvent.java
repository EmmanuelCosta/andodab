package fr.umlv.andodab.synchronization;

import fr.umlv.andodab.contentProvider.AndodabParameter;

/**
 * Created by Mathieu on 13/03/2015.
 */
public class ParameterCreationEvent extends ParameterAbstractEvent {
  private static final long serialVersionUID = -9153954698908381691L;

  /**
   * Constructor using a passed object as this event's target.
   */
  public ParameterCreationEvent() {
    super();
  }

  /**
   * Alternative constructor.
   * @param parameter the parameter to create.
   * @param time the time the event occurred.
   */
  public ParameterCreationEvent(AndodabParameter parameter, Long time) {
    super(parameter, time);
  }

  @Override
  public String getStringifiedValue() {
    return getParameter().getId() + ";"
      + getParameter().getKey() + ";"
      + getParameter().getMaster().getId() + ";"
      + getParameter().getSlave().getId() + ";"
      + getParameter().getType();
  }

  @Override
  public EventType getType() {
    return EventType.CREATION;
  }
}
