package fr.umlv.andodab.synchronization;

import android.content.Context;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Event writer class.
 * @author Mathieu Abou-Aichi.
 */
final class EventWriter {
  static final String FILENAME = "logs.txt";

  private EventWriter() {
    // do nothing
  }

  /**
   * Writes the corresponding event to the log output.
   * @param event the event to record.
   * @param context the context.
   * @throws IOException if opening the file or writing on it fails.
   */
  static void write(Event event, Context context) throws IOException {
    FileOutputStream fileOutputStream=null;
    Log.i("EventWriter", "Write method begin");
    try {
      fileOutputStream = context.openFileOutput(FILENAME, Context.MODE_APPEND);
      fileOutputStream.write(event.toString().getBytes());
    } catch(IOException ioe) {
      Log.w("EventWriter", "Error :" + ioe.toString());
    } finally {
      fileOutputStream.close();
    }
    Log.i("EventWriter","Write method end");
//    ContextWrapper contextWrapper = new ContextWrapper(context);
//    FileOutputStream fileOutputStream = contextWrapper.openFileOutput(FILENAME, 0);
//    fileOutputStream.write(event.toString().getBytes());
//    fileOutputStream.close();
  }
}
