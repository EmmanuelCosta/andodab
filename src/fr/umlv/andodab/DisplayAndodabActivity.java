package fr.umlv.andodab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import fr.umlv.andodab.synchronization.ClientSynchroActivity;


public class DisplayAndodabActivity extends Activity {

    private Menu myMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_andodab);

        LinearLayout layout = (LinearLayout)findViewById(R.id.layout);

        //Récupération données à afficher A FAIRE !!!

        //Fausses données pour tests
        Map<String, String> data = new HashMap<>();
        data.put("name", "Aslan");
        data.put("birthYear", "2012");

        TextView tx1;
        TextView tx2;

        for (Map.Entry<String, String> entry : data.entrySet())
        {
            tx1 = new TextView(this);
            tx2 = new TextView(this);
            tx1.setTop(20);
            tx2.setTop(20);
            tx1.setText(entry.getKey() + " : ");

         /*           android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/nom"
            android:id="@+id/nom"
            android:layout_below="@+id/titre"
            android:layout_alignParentLeft="true"
            android:layout_alignParentStart="true"
            android:layout_marginTop="20dp"


            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:inputType="number"
            android:ems="15"
            android:id="@+id/editNom"
            android:layout_marginLeft="20dp"
            android:layout_marginRight="20dp"
            android:layout_toRightOf="@+id/nom"
            android:layout_alignBottom="@+id/nom"

            */
            tx2.setText(entry.getValue());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            // lp.addRule(LinearLayout.VERTICAL, R.id.vue);

            //  tx1.setLayoutParams(lp);
            layout.addView(tx1);

            //  lp.addRule(LinearLayout.RIGHT_OF, tx1.getId());

            layout.addView(tx2);

        }
    }

}
