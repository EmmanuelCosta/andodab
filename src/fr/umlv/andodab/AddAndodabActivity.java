package fr.umlv.andodab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.AndodabObject;
import fr.umlv.andodab.contentProvider.AndodabParameter;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;
import fr.umlv.andodab.contentProvider.exception.DataBaseIntegrityException;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;

public class AddAndodabActivity extends Activity {

    private List<String> list;
    private HashMap<String, String> map = new HashMap<>();
    public static final String PARENT_ID = "fr.umlv.andodab.Activity_parent_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_andodab);

        //Initalisation du DAO, récupération du layout et de l'intent
        AndodabDAO dao = new AndodabDAO(getContentResolver(), this.getApplicationContext());
        LinearLayout layout = (LinearLayout) findViewById(R.id.add_layout);
        final Intent intent = getIntent();
        AndodabObject father = null;

        //Récupération de l'Id transmis par l'Intent depuis l'activité principale
        try {
            Logger.getAnonymousLogger().info(" ==== >>> ID = " + intent.getStringExtra(PARENT_ID));
            father = dao.getAndodabObectById(intent.getStringExtra(PARENT_ID));
        } catch (NotFoundElementException e) {
            e.printStackTrace();
        }

        //Récupération des paramètres du parent
        List<AndodabParameter> params = father.getParams();

        final Map<String, Spinner> allSpinner = new HashMap<>();

        //Ajout du TextView et EditText à la View pour le nom
        TextView label;
        final EditText nom;
        label = new TextView(this);
        nom = new EditText(this);

        label.setTop(20);
        nom.setTop(20);
        label.setText("Name : ");

        layout.addView(label);
        layout.addView(nom);

        TextView labelVal = new TextView(this);
        final EditText val = new EditText(this);
        //Ajout du Spinner pour le type à la View
        final Spinner type = new Spinner(this);
        ArrayAdapter<String> typeAdapater = null;
        if (father.getType().equals(AndodabObject.ObjectType.C) || father.getType().equals(AndodabObject.ObjectType.R)) {
            typeAdapater = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"Common"});
        } else {
            //Ajout du TextView et EditText à la View pour le nom
            if (father.getType().equals(AndodabObject.ObjectType.F)) {
                val.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                typeAdapater = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"Float"});
            } else if (father.getType().equals(AndodabObject.ObjectType.I)) {
                val.setInputType(InputType.TYPE_CLASS_NUMBER);
                typeAdapater = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"Integer"});
            } else if (father.getType().equals(AndodabObject.ObjectType.S)) {
                typeAdapater = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"String"});
            }

            labelVal.setTop(20);
            labelVal.setTop(20);
            labelVal.setText("Val : ");
            layout.addView(labelVal);
            layout.addView(val);
        }

        typeAdapater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        type.setAdapter(typeAdapater);
        type.setTop(20);

        label = new TextView(this);
        label.setText("Type : ");

        layout.addView(label);
        layout.addView(type);

        //Ajout du CheckBox pour l'option sealed à la View
        final CheckBox sealed = new CheckBox(this);
        sealed.setText("Sealed");


        //Ajout des parametres du parent à la View si il n'est pas sealed
        if (!father.isSealed() || father.getId().equals(dao.getRootID())) {

            map = new HashMap<>();

            for (AndodabParameter entry : params) {
                list = new ArrayList<>();

                label = new TextView(this);
                Spinner spinner = new Spinner(this);

                //Ajout du spinner dans la map de spinner (pour récupération de la valeur sélectionnée au moment de l'enregistrement de l'ojet
                allSpinner.put(entry.getKey(), spinner);

                label.setTop(20);
                spinner.setTop(20);
                label.setText(entry.getKey() + " : ");

                //Ajout valeur dans spinner/map
                list.add(entry.getSlave().getName());
                map.put(entry.getSlave().getName(), entry.getSlave().getId());

                String id = entry.getSlave().getId();

                //Récupération de tout les enfants de la valeur du paramètre du parent
                getChild(id);

                //Ajout du spinner dans la View
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layout.addView(label);
                layout.addView(spinner);
            }

            //Ajout de sealed dans la View
            sealed.setTop(20);
            layout.addView(sealed);
        }

        //Map de spinner des nouveaux paramètres
        final HashMap<EditText, Spinner> allNewParam = new HashMap<>();
        if (!father.isSealed() || dao.getRootID().equals(father.getId())) {
            //Bouton d'ajout de parametres
            Button buttonParam = new Button(this);
            buttonParam.setText("Add a parameter");
            layout.addView(buttonParam);

            buttonParam.setOnClickListener(new View.OnClickListener() {


                                               @Override
                                               public void onClick(View v) {
                                                   //Init
                                                   LinearLayout layout = (LinearLayout) findViewById(R.id.add_layout);

                                                   EditText label = new EditText(AddAndodabActivity.this);

                                                   list = new ArrayList<>();
                                                   Spinner spinner = new Spinner(AddAndodabActivity.this);

                                                   //Ajout du spinner dans la map de spinner (pour récupération de la valeur sélectionnée au moment de l'enregistrement de l'ojet
                                                   allNewParam.put(label, spinner);

                                                   label.setTop(20);
                                                   spinner.setTop(20);
                                                   label.setHint("Nom du parametre");

                                                   AndodabDAO dao = new AndodabDAO(getContentResolver(), getApplicationContext());

                                                   //Récupération de tout les objets de la base et ajout dans le spinner
                                                   for (LazyAndodabObject entry : dao.getAllLazyAndodabObject()) {

                                                       //Ajout valeur dans spinner/map
                                                       Logger.getAnonymousLogger().info("dans add la entry  est =" + (entry.getName() == null) + "m id est == " + (entry.getId() == null));
                                                       list.add(entry.getName());
                                                       map.put(entry.getName(), entry.getId());

                                                   }

                                                   //Ajout du spinner dans la View
                                                   ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(AddAndodabActivity.this, android.R.layout.simple_spinner_item, list);
                                                   dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                   spinner.setAdapter(dataAdapter);

                                                   LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                                                   layout.addView(label);
                                                   layout.addView(spinner);

                                               }
                                           }
            );
        }
        //Bouton de validation
        Button buttonValidate = new Button(this);
        buttonValidate.setText("Validate");
        layout.addView(buttonValidate);

        buttonValidate.setOnClickListener(new View.OnClickListener() {

                                              @Override
                                              public void onClick(View v) {
                                                  try {
                                                      //Init
                                                      AndodabDAO dao = new AndodabDAO(getContentResolver(), getApplicationContext());

                                                      String t = type.getSelectedItem().toString().substring(0, 1);

                                                      //Récupération du parent
                                                      AndodabObject father = dao.getAndodabObectById(intent.getStringExtra(PARENT_ID));


                                                      AndodabObject newSon = new AndodabObject(nom.getText().toString(), AndodabObject.ObjectType.valueOf(t), father.getId(), father.isSealed() ? true : sealed.isChecked());
                                                      if (!father.getType().equals(AndodabObject.ObjectType.C) && !father.getType().equals(AndodabObject.ObjectType.R)) {
                                                          newSon.setValue(val.getText().toString());
                                                          dao.insertAndodabObject(newSon);
                                                      } else {
                                                          AndodabParameter param;
                                                          AndodabObject obj;
                                                          if (!sealed.isChecked() && father.getType().equals(AndodabObject.ObjectType.R)) {
                                                              newSon.setSealed(false);
                                                          }
                                                          //Ajout des paramètres hérités à l'enfant si parent n'est pas sealed
                                                          if (!father.isSealed() && !father.getId().equals(dao.getRootID())) {
                                                              for (String key : allSpinner.keySet()) {
                                                                  obj = dao.getAndodabObectById(map.get(allSpinner.get(key).getSelectedItem()));

                                                                  if (obj.getType() == AndodabObject.ObjectType.C) {
                                                                      param = new AndodabParameter(key, obj.toLazyAndodabObject(), AndodabParameter.TypeOfSlaveParameter.ABSTRACT_COMMON, newSon.toLazyAndodabObject());
                                                                  } else {
                                                                      param = new AndodabParameter(key, obj.toLazyAndodabObject(), AndodabParameter.TypeOfSlaveParameter.DEFINE_OBJECT, newSon.toLazyAndodabObject());

                                                                  }
                                                                  newSon.addAndodabParameter(param);
                                                              }
                                                          }

                                                          //Création du nouvel objet
                                                          dao.insertAndodabObject(newSon);

                                                          //Récupération du nouvel objet
                                                          newSon = dao.getAndodabObectById(newSon.getId());

                                                          //Création et ajout des nouveaux paramètres à l'objet créé
                                                          for (EditText key : allNewParam.keySet()) {
                                                              obj = dao.getAndodabObectById(map.get(allNewParam.get(key).getSelectedItem()));

                                                              if (obj.getType() == AndodabObject.ObjectType.C) {
                                                                  param = new AndodabParameter(key.getText().toString(), obj.toLazyAndodabObject(), AndodabParameter.TypeOfSlaveParameter.ABSTRACT_COMMON, newSon.toLazyAndodabObject());
                                                              } else {
                                                                  param = new AndodabParameter(key.getText().toString(), obj.toLazyAndodabObject(), AndodabParameter.TypeOfSlaveParameter.DEFINE_OBJECT, newSon.toLazyAndodabObject());
                                                              }

                                                              dao.insertAndodabParameter(param);
                                                              newSon.addAndodabParameter(param);
                                                          }

                                                          //Update du nouvel objet pour y ajouter ses nouveaux paramètres
                                                          dao.updateAndodabObject(newSon);
                                                      }
                                                      //Message de création réussie
                                                      Toast.makeText(getApplicationContext(), "Object : " + dao.getAndodabObectById(newSon.getId()).getName() + " created.", Toast.LENGTH_LONG).show();

                                                  } catch (NotFoundElementException e) {
                                                      e.printStackTrace();
                                                  } catch (DataBaseIntegrityException e) {
                                                      e.printStackTrace();
                                                  } catch (IOException e) {
                                                      e.printStackTrace();
                                                  }
                                              }
                                          }
        );

    }

    //Méthode de récupération des enfants d'un objet
    public void getChild(String id) {
        AndodabDAO dao = new AndodabDAO(getContentResolver(), this.getApplicationContext());

        List<LazyAndodabObject> children = dao.getLazyAndodabObjectChildrenByParentId(id);
        if (children != null) {
            for (LazyAndodabObject obj : children) {
                list.add(obj.getName());
                map.put(obj.getName(), obj.getId());
                getChild(obj.getId());
            }
        }
    }


}

