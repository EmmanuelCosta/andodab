package fr.umlv.andodab.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Run the service ReceiverNotification when the os boot
 */
public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i=new Intent(context, ReceiverNotification.class);
        i.setAction(ReceiverNotification.ACTION_START);
        context.startService(i);
    }
}
