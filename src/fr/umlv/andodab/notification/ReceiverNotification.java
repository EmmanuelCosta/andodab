package fr.umlv.andodab.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import fr.umlv.andodab.MainActivity;
import fr.umlv.andodab.R;

public class ReceiverNotification extends Service {
    public static final String ACTION_START="start";
    public static final String ACTION_STOP="stop";
    private static Thread updateThread=null;

    public ReceiverNotification() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotification(String msg){
        final NotificationManager mNotification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Intent launchNotifiactionIntent = new Intent(this, MainActivity.class);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this,0, launchNotifiactionIntent,
                PendingIntent.FLAG_ONE_SHOT);

        //we used getNotifaction than build, because build method is not in API14
        Notification notification = new Notification.Builder(this)
                .setContentTitle("Andodab")
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pendingIntent).getNotification();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotification.notify(0, notification);
    }

    private void waitNotification(){
      try {
          WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
          WifiManager.MulticastLock multicastLock = wifi.createMulticastLock("multicastLock");
          multicastLock.setReferenceCounted(true);
          multicastLock.acquire();

          MulticastSocket socket = new MulticastSocket(9999);
          InetAddress group = InetAddress.getByName("224.0.0.3");
          socket.joinGroup(group);

          try{
              DatagramPacket packet;
              byte[] buf = new byte[256];
              packet = new DatagramPacket(buf, buf.length);
              socket.setSoTimeout(30000);
              socket.receive(packet);
              String received = new String(packet.getData());
              createNotification(received);
              socket.leaveGroup(group);
              socket.close();
              if (multicastLock != null) {
                  multicastLock.release();
                  multicastLock = null;
              }
          }catch(IOException e){
              Log.v("ReceiverNotification", e.toString());
          }
      }catch(Exception e){
          try {
              Log.v("ReceiverNotification", "wifi don't work : "+e.toString());
              Thread.sleep(30000);
          }catch(InterruptedException ie){
              Thread.currentThread().interrupt();
          }
      }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent==null)return 0;
        if(intent.getAction().equals(ACTION_START)){
            if(updateThread!=null)return 0;
            Log.v("ReceiverNotification","New service receiver started");
            updateThread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(! Thread.interrupted()){
                        waitNotification();
                    }
                }
            });
            updateThread.start();
        }
        else if (intent.getAction().equals(ACTION_STOP)){
            Log.v("ReceiverNotification","Service receiver stop");
            if(updateThread!=null)updateThread.interrupt();
            updateThread=null;
        }
        return 0;
    }

    @Override
    public void onDestroy() {
        if(updateThread!=null)updateThread.interrupt();
    }
}
