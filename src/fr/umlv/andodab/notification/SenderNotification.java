package fr.umlv.andodab.notification;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SenderNotification extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SEND = "fr.umlv.andodab.notification.action.SEND";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "fr.umlv.andodab.notification.extra.PARAM1";

    /**
     * Starts this service to perform action Send with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSend(Context context, String param1) {
        Intent intent = new Intent(context, SenderNotification.class);
        intent.setAction(ACTION_SEND);
        intent.putExtra(EXTRA_PARAM1, param1);
        context.startService(intent);
    }

    public SenderNotification() {
        super("SenderNotification");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEND.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                handleActionSend(param1);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSend(String param1) {
      Log.v("SenderNotification", "operation to send msg begin");
      try {
            MulticastSocket socket = new MulticastSocket();
            byte buff[] = param1.getBytes();
            DatagramPacket packet = new DatagramPacket(buff, buff.length, InetAddress.getByName("224.0.0.3"), 9999);
            socket.send(packet);
            socket.close();
        } catch (Exception e) {
          Log.v("SenderNotification","msg impossible to be send : "+e.toString());
        }
      Log.v("SenderNotification", "operation to send msg finish");

    }
}
