package fr.umlv.andodab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.logging.Logger;

import fr.umlv.andodab.contentProvider.AndodabContentProvider;
import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.AndodabObject;
import fr.umlv.andodab.contentProvider.AndodabParameter;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;
import fr.umlv.andodab.contentProvider.exception.DataBaseIntegrityException;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;
import fr.umlv.andodab.view.DisplayAndodabObject;


public class InsertActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //TODO: handle events
        if (id == R.id.context_menu_add_object) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void insertAndodab(View v){

        try {
            AndodabDAO dao = new AndodabDAO(getContentResolver(),getApplicationContext());

            AndodabObject andodabObject= new AndodabObject("ORANGE", AndodabObject.ObjectType.C, dao.getRootID(),false);


            Logger.getAnonymousLogger().info(dao.getRootID());
            dao.insertAndodabObject(andodabObject);
//            AndodabObject lazy = dao.getAndodabObectById((long) 4);
//            lazy.setParentId(new Long(1));
//            dao.updateAndodabObject(lazy);
        } catch (DataBaseIntegrityException e) {
            e.printStackTrace();
        } catch (NotFoundElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateAndodab(View v){

        try {
            AndodabDAO dao = new AndodabDAO(getContentResolver(),getApplicationContext());
            AndodabParameter param = dao.getAndodabParameterById( "3");
            Logger.getAnonymousLogger().info("Param ="+param.getId()+" key ="+param.getKey());

            param.setSlave(dao.getLazyAndodabObjectById("1"));
            dao.updateAndodabParameter(param);
        }  catch (NotFoundElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchToRealMain(View v){
        Intent intent = new Intent(this, DisplayAndodabObject.class);
//
        startActivity(intent);
    }
}
