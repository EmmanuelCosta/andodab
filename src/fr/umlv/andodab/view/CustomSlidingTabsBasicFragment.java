/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.umlv.andodab.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import fr.umlv.andodab.R;
import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.AndodabParameter;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;


public class CustomSlidingTabsBasicFragment extends Fragment {

  static final String LOG_TAG = "SlidingTabsBasicFragment";

  /**
   * A custom {@link android.support.v4.view.ViewPager} title strip which looks much like Tabs present in Android v4.0 and
   * above, but is designed to give continuous feedback to the user when scrolling.
   */
  private FirstStageCustomTabLayout mCustomSlidingTabLayout;
  private SecondStageCustomTabLayout mCustomSlidingTabLayout2;
  private ThirdStageCustomTabLayout mCustomSlidingTabLayout3;

  /**
   * A {@link android.support.v4.view.ViewPager} which will be used in conjunction with the {@link CustomSlidingTabLayout} above.
   */
  private ViewPager mViewPager;

  private ViewPager mViewPager2;
  private ViewPager mViewPager3;

  private AndodabDAO andodabDAO;

  public static String currentObjectID = null;

  /**
   * Inflates the {@link android.view.View} which will be displayed by this {@link android.support.v4.app.Fragment}, from the app's
   * resources.
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    andodabDAO = new AndodabDAO(getActivity().getContentResolver(), getActivity().getApplicationContext());
    currentObjectID = andodabDAO.getRootID();
    return inflater.inflate(R.layout.fragment_sample, container, false);
  }

  // BEGIN_INCLUDE (fragment_onviewcreated)

  /**
   * This is called after the {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)} has finished.
   * Here we can pick out the {@link android.view.View}s we need to configure from the content view.
   * <p/>
   * We set the {@link android.support.v4.view.ViewPager}'s adapter to be an instance of { SamplePagerAdapter}. The
   * {@link CustomSlidingTabLayout} is then given the {@link android.support.v4.view.ViewPager} so that it can populate itself.
   *
   * @param view View created in {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)}
   */
  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    // Get the ViewPager and set it's PagerAdapter so that it can display items
    mViewPager = (ViewPager) view.findViewById(R.id.viewpager);

    //STAGE ADAPTER
    FirstStageAdapter firstStageAdapter = new FirstStageAdapter();
    SecondStageAdapter secondStageAdapter = new SecondStageAdapter();
    ThirdStageAdapter thirdStageAdapter = new ThirdStageAdapter();

    firstStageAdapter.setSecondStageAdapter(secondStageAdapter);
    secondStageAdapter.setFirstStageAdapter(firstStageAdapter);
    secondStageAdapter.setThirdStageAdapter(thirdStageAdapter);

    thirdStageAdapter.setSecondStageAdapter(secondStageAdapter);


    mViewPager.setAdapter(firstStageAdapter);
    mViewPager.setVisibility(View.GONE);

    andodabDAO = new AndodabDAO(getActivity().getContentResolver(), getActivity().getApplicationContext());
    mViewPager2 = (ViewPager) view.findViewById(R.id.viewpager2);
    mViewPager2.setAdapter(secondStageAdapter);


    mViewPager3 = (ViewPager) view.findViewById(R.id.viewpager3);
    mViewPager3.setAdapter(thirdStageAdapter);
    mViewPager3.setVisibility(View.GONE);

    // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
    // it's PagerAdapter set.
    mCustomSlidingTabLayout = (FirstStageCustomTabLayout) view.findViewById(R.id.sliding_tabs);

    mCustomSlidingTabLayout.setViewPager(mViewPager);
    mCustomSlidingTabLayout.setFirstStageAdapter(firstStageAdapter);


    mCustomSlidingTabLayout3 = (ThirdStageCustomTabLayout) view.findViewById(R.id.sliding_tabs3);
    mCustomSlidingTabLayout3.setViewPager(mViewPager3);
    mCustomSlidingTabLayout3.setThirdStageAdapter(thirdStageAdapter);

    mCustomSlidingTabLayout2 = (SecondStageCustomTabLayout) view.findViewById(R.id.sliding_tabs2);
    mCustomSlidingTabLayout2.setViewPager(mViewPager2);
    mCustomSlidingTabLayout2.setSecondStageAdapter(secondStageAdapter);
    mCustomSlidingTabLayout2.setCustomTabColorizer(new CustomSlidingTabLayout.TabColorizer() {
      @Override
      public int getIndicatorColor(int position) {
        return Color.BLUE;
      }

      @Override
      public int getDividerColor(int position) {
        return Color.BLACK;
      }
    });

    mCustomSlidingTabLayout.setSecondStageCustomTabLayout(mCustomSlidingTabLayout2);
    mCustomSlidingTabLayout2.setFirstCustomSlidingTabLayout(mCustomSlidingTabLayout);
    mCustomSlidingTabLayout2.setThirdCustomSlidingTabLayout(mCustomSlidingTabLayout3);
    mCustomSlidingTabLayout3.setCustomSlidingTabLayout(mCustomSlidingTabLayout2);
    mCustomSlidingTabLayout3.setCustomTabColorizer(new CustomSlidingTabLayout.TabColorizer() {
      @Override
      public int getIndicatorColor(int position) {
        return Color.GREEN;
      }

      @Override
      public int getDividerColor(int position) {
        return Color.BLACK;
      }
    });
  }

  public void deleteSelectedItem() {

    try {
      AndodabDAO dao = new AndodabDAO(getActivity().getContentResolver(), getActivity().getApplicationContext());
      if (currentObjectID.equals(dao.getRootID())) {
        Toast.makeText(getActivity().getApplicationContext(), "IMPOSSIBLE DE SUPPRIMER LE ROOT", Toast.LENGTH_SHORT).show();
      } else {
        dao.deleteAndodabObjectById(currentObjectID);
        mCustomSlidingTabLayout2.refreshDrawableState();
        mCustomSlidingTabLayout3.refreshDrawableState();
      }
    } catch (IOException e) {
      Toast.makeText(getActivity().getApplicationContext(), "UNE ERREUR EST SURVENUE LORS DE LA SUPPRESSION ", Toast.LENGTH_SHORT).show();
    }
  }


  /**
   * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
   * The individual pages are simple and just display two lines of text. The important section of
   * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
   * {@link CustomSlidingTabLayout}.
   */
  class FirstStageAdapter extends PagerAdapter {
    private AndodabDAO andodabDAO;
    private List<LazyAndodabObject> lazyList;
    private String current_id;
    private Activity activity;
    private SecondStageAdapter secondStageAdapter;
    private SparseArray<View> views = new SparseArray<>();

    public FirstStageAdapter() {
      this.andodabDAO = new AndodabDAO(getActivity().getContentResolver(), getActivity().getApplicationContext());
      current_id = andodabDAO.getRootID();
      this.lazyList = new ArrayList<>();
      try {
        this.lazyList.add(this.andodabDAO.getLazyAndodabObjectById(current_id));
      } catch (NotFoundElementException e) {
        e.printStackTrace();
      }
      this.activity = getActivity();
    }

    public FirstStageAdapter(Context context) {
      this.andodabDAO = new AndodabDAO(context.getContentResolver(), context);
      current_id = "-1";
      lazyList = new ArrayList<>();
      this.activity = (Activity) context;

    }

    public void setSecondStageAdapter(SecondStageAdapter secondStageAdapter) {
      this.secondStageAdapter = secondStageAdapter;
    }

    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {

            try {
                return this.lazyList.size();
            } catch (IllegalArgumentException e) {
                return 0;
            }

    }

    public void setCurrent_id(String current_id) {
      this.current_id = current_id;
      this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id);

    }


    public void setSuperCurentId(String id) {
      try {
        LazyAndodabObject lazyAndodabObject = this.andodabDAO.getLazyAndodabObjectById(id);
        this.current_id = lazyAndodabObject.getParentId();
        this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(id);
      } catch (NotFoundElementException e) {
        e.printStackTrace();
      }
    }

    public SecondStageAdapter getSecondStageAdapter() {
      return secondStageAdapter;
    }

    public AndodabDAO getAndodabDAO() {
      return andodabDAO;
    }

    /**
     * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
     * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
     */
    @Override
    public boolean isViewFromObject(View view, Object o) {
      return o == view;
    }


    /**
     * Return the title of the item at {@code position}. This is important as what this method
     * returns is what is displayed in the {@link CustomSlidingTabLayout}.
     * <p/>
     * Here we construct one using the position value, but for real application the title should
     * refer to the item's contents.
     */
    @Override
    public CharSequence getPageTitle(int position) {
      Logger.getAnonymousLogger().warning("== call getPageTitle ======= ");
      //List<LazyAndodabObject> lazyAndodabObjectList = andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id);
      if (lazyList.size() > 1)
        return lazyList.get(position).getName();
      return lazyList.get(0).getName();
    }

    public String getIdAtPosition(int i) {
      return lazyList.get(i).getId();
    }

    /**
     * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
     * inflate a layout from the apps resources and then change the text view to signify the position.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
      // Inflate a new layout from our resources
      View view = this.activity.getLayoutInflater().inflate(R.layout.pager_item,
        container, false);
      // Add the newly created View to the ViewPager
      container.addView(view);
      Logger.getAnonymousLogger().info("click dans first sstage\n");
      // Retrieve a TextView from the inflated View, and update it's text
      TextView id = (TextView) view.findViewById(R.id.item_resultId);

      TextView name = (TextView) view.findViewById(R.id.item_resultName);

      TextView parent_id = (TextView) view.findViewById(R.id.item_resultParentID);

      LazyAndodabObject andodabObectById = this.lazyList.get(position);
      id.setTextSize(15);
      id.setText(andodabObectById.getId());
      name.setText(andodabObectById.getName());
      parent_id.setText(andodabObectById.getParentId());

      // Return the View
      views.put(position, view);
      return view;
    }

    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link android.view.View}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      View view = (View) object;
      container.removeView(view);
      //Log.i(LOG_TAG, "destroyItem() [position: " + position + "]");
//            views.remove(position);
      views.remove(position);
      view = null;
    }

    @Override
    public void notifyDataSetChanged() {
      int key = 0;
      for (int i = 0; i < views.size(); i++) {
        key = views.keyAt(i);
        View view = views.get(key);
        Logger.getAnonymousLogger().info(" EXEDED 1 ");

      }
      super.notifyDataSetChanged();
    }
  }

  /**
   * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
   * The individual pages are simple and just display two lines of text. The important section of
   * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
   * {@link CustomSlidingTabLayout}.
   */
  class SecondStageAdapter extends PagerAdapter {
    private AndodabDAO andodabDAO;
    private List<LazyAndodabObject> lazyList;
    String parent_id;
    private FirstStageAdapter firstStageAdapter;
    private ThirdStageAdapter thirdStageAdapter;
    private Activity activity;
    private SparseArray<View> views = new SparseArray<>();

    public SecondStageAdapter() {
      this.andodabDAO = new AndodabDAO(getActivity().getContentResolver(), getActivity().getApplicationContext());
      parent_id = "-1";
      this.lazyList = new ArrayList<>();

      this.activity = getActivity();

    }

    public SecondStageAdapter(Context context) {
      this.andodabDAO = new AndodabDAO(context.getContentResolver(), context);
      parent_id = "-1";
      lazyList = new ArrayList<>();
      this.activity = (Activity) context;

    }


    public void setFirstStageAdapter(FirstStageAdapter firstStageAdapter) {
      this.firstStageAdapter = firstStageAdapter;
    }

    public void setThirdStageAdapter(ThirdStageAdapter thirdStageAdapter) {
      this.thirdStageAdapter = thirdStageAdapter;
    }

    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {
      try {
        return andodabDAO.getLazyAndodabObjectChildrenByParentId(parent_id).size();
      } catch (IllegalArgumentException e) {
        return 0;
      }

    }

    public void setParent_id(String parent_id) {
      this.parent_id = parent_id;
      this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(parent_id);

    }

    public void setCurrent_id(String parent_id) {
      try {
        LazyAndodabObject lazyAndodabObject = this.andodabDAO.getLazyAndodabObjectById(parent_id);
        this.parent_id = lazyAndodabObject.getParentId();
        this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(parent_id);
      } catch (NotFoundElementException e) {
        e.printStackTrace();
      }
    }

    public AndodabDAO getAndodabDAO() {
      return andodabDAO;
    }

    public FirstStageAdapter getFirstStageAdapter() {
      return firstStageAdapter;
    }

    public ThirdStageAdapter getThirdStageAdapter() {
      return thirdStageAdapter;
    }

    /**
     * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
     * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
     */
    @Override
    public boolean isViewFromObject(View view, Object o) {
      return o == view;
    }

        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link CustomSlidingTabLayout}.
         * <p/>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            try {
                return lazyList.get(position).getName();
            }catch (IndexOutOfBoundsException e){
                return "";
            }
        }


    /**
     * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
     * inflate a layout from the apps resources and then change the text view to signify the position.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
      // Inflate a new layout from our resources
      View view = this.activity.getLayoutInflater().inflate(R.layout.pager_item,
        container, false);
      // Add the newly created View to the ViewPager
      container.addView(view);

      // Retrieve a TextView from the inflated View, and update it's text
      TextView id = (TextView) view.findViewById(R.id.item_resultId);

      TextView name = (TextView) view.findViewById(R.id.item_resultName);

      TextView parent_id = (TextView) view.findViewById(R.id.item_resultParentID);


      if (!lazyList.isEmpty()) {
        LazyAndodabObject andodabObectById = this.lazyList.get(position);
        id.setTextSize(15);
        currentObjectID = andodabObectById.getId();
        id.setText(currentObjectID);
        name.setText(andodabObectById.getName());
        parent_id.setText(andodabObectById.getParentId());
      }

      final List<AndodabParameter> parameters = new ArrayList<>();
      try {
        parameters.addAll(
          andodabDAO.getParametersByAndodabObjectId(this.lazyList.get(position).getId()));
      } catch (NotFoundElementException e) {
        // do nothing, we already have an empty list
      }

      LinearLayout layout = (LinearLayout) view.findViewById(R.id.paramLayout);
      final List<AndodabParameter> updatedParameterList = new ArrayList<>();
      for (final AndodabParameter parameter : parameters) {
        updatedParameterList.add(parameter);
        TextView paramName = new TextView(activity);
        paramName.setText(parameter.getKey());

        LinearLayout internalLinearLayout = new LinearLayout(activity);
        internalLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        internalLinearLayout.addView(paramName);

        if (parameter.getMaster().isSealed()) {
          TextView textView = new TextView(activity);
          textView.setText(parameter.getSlave().getName());
          internalLinearLayout.addView(textView);
        } else {
          Spinner spinner = new Spinner(activity);
          spinner.setTop(20);

          List<LazyAndodabObject> list = new ArrayList<>();
          list.add(parameter.getSlave());
          for (LazyAndodabObject entry : andodabDAO.getAllLazyAndodabObject()) {
            if (!list.contains(entry)
              && entry.getParentId().equals(parameter.getSlave().getParentId())) {
              list.add(entry);
            }
          }
          ArrayAdapter<LazyAndodabObject> dataAdapter =
            new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, list);
          dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          spinner.setAdapter(dataAdapter);
          spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              // we set the new param object here (new slave for the master)
              // so that we can safely update later, when the user clicks on the validation button.
              updatedParameterList.get(position)
                .setSlave(((ArrayAdapter<LazyAndodabObject>) parent.getAdapter()).getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
              // nothing should happen
            }
          });
          internalLinearLayout.addView(spinner);
        }
        layout.addView(internalLinearLayout);
      }

      Button validationButton = (Button) view.findViewById(R.id.paramValidationButton);
      validationButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          for (AndodabParameter param : updatedParameterList) {
            try {
              andodabDAO.updateAndodabParameter(param);
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
      });
      views.put(position, view);
      return view;
    }

    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link android.view.View}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      View view = (View) object;
      container.removeView(view);
      views.remove(position);
      view = null;
    }

    @Override
    public void notifyDataSetChanged() {
      int key = 0;
      for (int i = 0; i < views.size(); i++) {
        key = views.keyAt(i);
        View view = views.get(key);
        Logger.getAnonymousLogger().info(" EXECUTE notify SECOND ");

      }
      super.notifyDataSetChanged();
    }

    public String getIdAtPosition(int i) {
      return lazyList.get(i).getId();
    }


    public void display() {
      for (LazyAndodabObject lazyAndodabObject : lazyList)
        System.out.println(lazyAndodabObject.getName() + " \n");
    }
  }

  /**
   * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
   * The individual pages are simple and just display two lines of text. The important section of
   * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
   * {@link CustomSlidingTabLayout}.
   */
  class ThirdStageAdapter extends PagerAdapter {
    private Activity activity;
    private AndodabDAO andodabDAO;
    private List<LazyAndodabObject> lazyList;
    String current_id;
    private SecondStageAdapter secondStageAdapter;
    private SparseArray<View> views = new SparseArray<>();

    public ThirdStageAdapter() {
      this.activity = getActivity();
      this.andodabDAO = new AndodabDAO(this.activity.getContentResolver(), getActivity().getApplicationContext());
      current_id = "-1";
      this.lazyList = new ArrayList<>();

    }

    public ThirdStageAdapter(Context context) {
      this.andodabDAO = new AndodabDAO(context.getContentResolver(), context);
      current_id = "-1";
      lazyList = new ArrayList<>();
      this.activity = (Activity) context;

    }

    public void setSecondStageAdapter(SecondStageAdapter secondStageAdapter) {
      this.secondStageAdapter = secondStageAdapter;
    }

    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {

            try {
                return andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id).size();
            } catch (IllegalArgumentException e) {
                return 0;
            }
        }

    public void setParent_id(String parent_id) {
      this.current_id = parent_id;
      this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(parent_id);
    }

    /**
     * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
     * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
     */
    @Override
    public boolean isViewFromObject(View view, Object o) {
      return o == view;
    }

    /**
     * Return the title of the item at {@code position}. This is important as what this method
     * returns is what is displayed in the {@link CustomSlidingTabLayout}.
     * <p/>
     * Here we construct one using the position value, but for real application the title should
     * refer to the item's contents.
     */
    @Override
    public CharSequence getPageTitle(int position) {
      return lazyList.get(position).getName();
    }


    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link android.view.View}.
     */
    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link android.view.View}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      View view = (View) object;
      container.removeView((View) view);
      views.remove(position);
      view = null;
    }

    @Override
    public void notifyDataSetChanged() {
      int key = 0;
      for (int i = 0; i < views.size(); i++) {
        key = views.keyAt(i);
        View view = views.get(key);
        Logger.getAnonymousLogger().info(" EXEDED 3 ");

      }
      super.notifyDataSetChanged();
    }

    /**
     * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
     * inflate a layout from the apps resources and then change the text view to signify the position.
     */
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
      Logger.getAnonymousLogger().info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> in instantianteItem");
      // Inflate a new layout from our resources
      View view = this.activity.getLayoutInflater().inflate(R.layout.pager_item,
        container, false);
      // Add the newly created View to the ViewPager
      container.addView(view);

      // Return the View
      views.put(position, view);
      return view;
    }


    public String getIdAtPosition(int i) {
      return lazyList.get(i).getId();
    }

    public SecondStageAdapter getSecondStageAdapter() {
      return secondStageAdapter;
    }

    public String getCurrent_id() {
      return current_id;
    }
  }

  public static String getCurrentObjectID() {
    return currentObjectID;
  }

}
