package fr.umlv.andodab.view;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.logging.Logger;

import fr.umlv.andodab.R;
import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;

/**
 * Created by emmanuel on 14/03/2015.
 */

class GenericAdapter extends PagerAdapter {
    private AndodabDAO andodabDAO;
    private List<LazyAndodabObject> lazyList;
    String current_id;

    public GenericAdapter(AndodabDAO dao) {
        this.andodabDAO = dao;
        current_id = andodabDAO.getRootID();
        this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id);
    }

    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {

        return andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id).size();

    }

    public void setCurrent_id(String current_id) {
        this.current_id = current_id;
        this.lazyList = this.andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id);
    }

    /**
     * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
     * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
     */
    @Override
    public boolean isViewFromObject(View view, Object o) {
        return o == view;
    }

    // BEGIN_INCLUDE (pageradapter_getpagetitle)

    /**
     * Return the title of the item at {@code position}. This is important as what this method
     * returns is what is displayed in the {@link CustomSlidingTabLayout}.
     * <p/>
     * Here we construct one using the position value, but for real application the title should
     * refer to the item's contents.
     */
    @Override
    public CharSequence getPageTitle(int position) {
        Logger.getAnonymousLogger().warning("== call getPageTitle ======= ");
        List<LazyAndodabObject> lazyAndodabObjectList = andodabDAO.getLazyAndodabObjectChildrenByParentId(current_id);

        return lazyAndodabObjectList.get(position).getName();
    }
    // END_INCLUDE (pageradapter_getpagetitle)

    /**
     * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
     * inflate a layout from the apps resources and then change the text view to signify the position.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // Inflate a new layout from our resources
//        View view = getActivity().getLayoutInflater().inflate(R.layout.pager_item,
//                container, false);
//        // Add the newly created View to the ViewPager
//        container.addView(view);
//
//        // Retrieve a TextView from the inflated View, and update it's text
//        TextView id = (TextView) view.findViewById(R.id.item_resultId);
//
//        TextView name = (TextView) view.findViewById(R.id.item_resultName);
//
//        TextView parent_id = (TextView) view.findViewById(R.id.item_resultParentID);
//
//        LazyAndodabObject andodabObectById = this.lazyList.get(position);
//        id.setTextSize(15);
//        id.setText(andodabObectById.getId());
//        name.setText(andodabObectById.getName());
//        parent_id.setText(andodabObectById.getParentId());
//
//        //  Log.i(LOG_TAG, "instantiateItem() [position: " + position + "]");
//
//        // Return the View
        return null;
    }

    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link android.view.View}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        //Log.i(LOG_TAG, "destroyItem() [position: " + position + "]");
    }

}

