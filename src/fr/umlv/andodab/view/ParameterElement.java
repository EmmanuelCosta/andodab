package fr.umlv.andodab.view;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import fr.umlv.andodab.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParameterElement extends Fragment {
  private String name;
  private String value;
  private String paramId;

  public ParameterElement() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_parameter_element, container, false);
  }


  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    TextView parameterName = (TextView) view.findViewById(R.id.parameterName);
    parameterName.setText(name);
    TextView parameterValue = (TextView) view.findViewById(R.id.parameterValue);
    parameterValue.setText(value);
    parameterValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        value = ((TextView) v).getText().toString();
      }
    });
  }

  public String getParamId() {
    return paramId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }
}
