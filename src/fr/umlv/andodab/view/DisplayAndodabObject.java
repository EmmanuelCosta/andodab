package fr.umlv.andodab.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.logging.Logger;

import fr.umlv.andodab.AddAndodabActivity;
import fr.umlv.andodab.R;
import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.synchronization.ClientSynchroActivity;
import fr.umlv.andodab.synchronization.ServerSynchroActivity;

/**
 * Created by Alexandre on 13/03/2015.
 */
public class DisplayAndodabObject extends FragmentActivity {

    public static final String TAG = "MainActivity";
    public CustomSlidingTabsBasicFragment fragment;
    // Whether the Log Fragment is currently shown
    private boolean mLogShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_general_layout);
        Logger.getAnonymousLogger().warning("======================= LAUNCH THE ON CREATE  =============");
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            fragment = new CustomSlidingTabsBasicFragment();
            transaction.replace(R.id.sample_content_fragment, fragment);
            transaction.commit();
        }
    }

    public void deleteAndodabObject(View v) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_display_andodab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu_add_object:
                Intent addObjectIntent = new Intent(getApplicationContext(), AddAndodabActivity.class);
//              try {
//                AndodabDAO andodoabDAO = new AndodabDAO(getContentResolver(), getApplicationContext());
//              } catch (IOException e) {
//                e.printStackTrace();
//              }
              addObjectIntent.putExtra(AddAndodabActivity.PARENT_ID, CustomSlidingTabsBasicFragment.getCurrentObjectID());
                startActivityForResult(addObjectIntent, 0);
                return true;
            case R.id.context_menu_sync_master:
                Intent serverSyncIntent = new Intent(getApplicationContext(), ServerSynchroActivity.class);
                startActivityForResult(serverSyncIntent, 0);
                return true;
            case R.id.context_menu_sync_slave:
                Intent clientSyncIntent = new Intent(getApplicationContext(), ClientSynchroActivity.class);
                startActivityForResult(clientSyncIntent, 0);
                return true;
            case R.id.context_menu_delete:
                fragment.deleteSelectedItem();
                return true;
        }
        return false;
    }

}
