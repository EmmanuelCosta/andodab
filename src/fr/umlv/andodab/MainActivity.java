package fr.umlv.andodab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import fr.umlv.andodab.contentProvider.AndodabDAO;
import fr.umlv.andodab.contentProvider.AndodabObject;
import fr.umlv.andodab.contentProvider.AndodabParameter;
import fr.umlv.andodab.contentProvider.LazyAndodabObject;
import fr.umlv.andodab.contentProvider.exception.NotFoundElementException;

public class MainActivity extends Activity {

    private AndodabDAO andodabDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        andodabDAO = new AndodabDAO(getContentResolver(),getApplicationContext());

    }

    public void showAll(View v) {
        List<LazyAndodabObject> lazyList = this.andodabDAO
                .getAllLazyAndodabObject();
        for (LazyAndodabObject lazy : lazyList) {
            Toast.makeText(
                    getApplicationContext(),
                    " ID :" + lazy.getId() + " Name: " + lazy.getName()
                            + " Type : " + lazy.getType().toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void showById(View v) {
        EditText idView = (EditText) findViewById(R.id.idofAndoObject);

        try {
            AndodabObject andodabObject = this.andodabDAO
                    .getAndodabObectById(idView.getText().toString());
            Toast.makeText(
                    getApplicationContext(),
                    " ID :" + andodabObject.getId() + "\n Name: "
                            + andodabObject.getName() + " \nType : "
                            + andodabObject.getType().toString()
                            + " \nisSealed : " + andodabObject.isSealed()
                            + " \nparent_id : " + andodabObject.getParentId(),
                    Toast.LENGTH_SHORT).show();

            if(andodabObject.isValueDefine()){
                Toast.makeText(getApplicationContext()," Value : "+andodabObject.getValue(),Toast.LENGTH_LONG).show();
            }
            List<AndodabParameter> params = andodabObject.getParams();
            if (!params.isEmpty()) {
                for (AndodabParameter andodabParameter : params) {
                    Toast.makeText(
                            getApplicationContext(),
                            " ID_PARAM :" + andodabParameter.getId() + " \nKey_PARAM: "
                                    + andodabParameter.getKey() + " \nType_PARAM : "
                                    + andodabParameter.getType().toString()
                                    + " \nslave_id_PARAM : " + andodabParameter.getSlave().getId(),
                            Toast.LENGTH_LONG).show();
                }
            }

        } catch (NotFoundElementException e) {
            e.printStackTrace();
        }

    }

    public void showDirectChild(View v) {
        EditText idView = (EditText) findViewById(R.id.idOfParent);
        List<LazyAndodabObject> lazyList = this.andodabDAO
                .getLazyAndodabObjectChildrenByParentId((idView.getText().toString()));
        for (LazyAndodabObject lazy : lazyList) {
            Toast.makeText(
                    getApplicationContext(),
                    " ID :" + lazy.getId() + " Name: " + lazy.getName()
                            + " Type : " + lazy.getType().toString(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteById(View v) {
            EditText idView = (EditText) findViewById(R.id.idofAndoObject);
//            andodabDAO.deleteAndodabObjectById((idView.getText().toString()));
    }

    public void deleteParameterById(View v) {


            EditText idView = (EditText) findViewById(R.id.idofAndoObject);
        try {
            andodabDAO.deleteAndodabParameterById((idView.getText().toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void insertActivity(View v){
        Intent intent = new Intent(this, InsertActivity.class);

        startActivity(intent);
    }

}


